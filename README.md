# <img src = "MrSnowman/resources/img/icon.png" width = 200 height = 200>
![C++](https://img.shields.io/badge/C%2B%2B-green) ![Qt6](https://img.shields.io/badge/Qt6-blue)
# Project MrSnowman

A 2D platformer where you help a snowman on his perilous journey!

___
### :wrench: How to run the game:
- <b> Run release version:</b><br>

Download the latest release version (for Windows only) from [this link](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/04-MrSnowman/-/releases/v1.0) , unpack the downloaded archive, and run the included executable file.
If the first link doesn't work, you can download the release build from [here](https://drive.google.com/file/d/1VVZpB5R5Ogud75BiETlxLXRWBTnf_yKW/view?usp=sharing)

- <b> Run from source code:</b><br>

Download and install the [Qt](https://www.qt.io/) library (don't forget to check Qt Multimedia library also!) and Qt Creator. <br>
After cloning the repository to a directory of your choosing, navigate to the MrSnowman folder. <br>
Inside you will find MrSnowman.pro file. Open it with Qt Creator. <br>
Click on the green Run button you will see in the bottom left. This will build the project and run the game.
<br><br>

___
### :video_game: Tutorial:
- [How to play](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/04-MrSnowman/-/wikis/Tutorial)

___
### :movie_camera: Demo video:
- [Demo video](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/04-MrSnowman/-/wikis/Demo%20snimak)

## Developers

- [Vukan Antić, 225/2018](https://gitlab.com/VukanAntic)
- [Tatjana Knežević, 218/2018](https://gitlab.com/TatjanaKnezevic)
- [Divna Mićić, 128/2018](https://gitlab.com/Divna99)
- [Aleksandar Šarbajić, 145/2018](https://gitlab.com/mi18145)
