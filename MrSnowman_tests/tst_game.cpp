#include <catch2/catch.hpp>
#include "../MrSnowman/code/game.h"
#include <iostream>

TEST_CASE("Game","[game]") {
    SECTION("Kada se napravi nova instanca igre njeno inicijalno stanje je READY"){

        Game* g;
        game_state result = READY;

        g = Game::instance();

        REQUIRE(result == g->getCurrentState());

    }

    SECTION("Kada je igrica zapoceta prikazuje se dijalog i njeno stanje je DIALOGUE"){

        Game* g;
        game_state result = DIALOGUE;

        g = Game::instance();
        g->start_game();

        REQUIRE(result == g->getCurrentState());

    }

    SECTION("Nakon otvaranja menija stanje igrice se nalazi u READY"){

        Game* g;
        g = Game::instance();
        game_state result = READY;

        g->open_menu();

        REQUIRE(result == g->getCurrentState());

    }

    SECTION("Ako je pozvana funkcija open_menu, levelID je postavljen na 1 tj. na pocetni nivo"){

        Game* g;
        unsigned result = 1;

        g = Game::instance();
        g->open_menu();

        REQUIRE(result == g->getLevelID());

    }

    SECTION("Nakon poziva funkcije play_music muzika treba da ne bude pauzirana"){

        Game* g;
        bool result = false;

        g = Game::instance();
        g->play_music();

        REQUIRE(result == g->musicPaused);

    }

    SECTION("Ako je stanje igrice RUNNING nakon poziva funkcije pause_game stanje se menja u PAUSE"){

        Game* g;
        game_state result = PAUSE;

        g = Game::instance();
        g->setCurrentState(RUNNING);
        g->pause_game();

        REQUIRE(result == g->getCurrentState());

    }

    SECTION("Ako je stanje igrice PAUSE nakon poziva funkcije pause_game stanje se menja u RUNNING"){

        Game* g;
        game_state result = RUNNING;

        g = Game::instance();
        g->setCurrentState(PAUSE);
        g->pause_game();

        REQUIRE(result == g->getCurrentState());

    }

    SECTION("Ako je igrica trenutno pauzirana i pritisnemo Escape, igrica se nastavlja i njeno stanje je RUNNING"){

        Game* g;
        game_state result = RUNNING;

        g = Game::instance();
        g->setCurrentState(PAUSE);
        g->keyPressEvent(new QKeyEvent (QEvent::KeyPress,Qt::Key_Escape,Qt::NoModifier,"esc"));

        REQUIRE(result == g->getCurrentState());

    }

    SECTION("Ako se u igri prikazuje dijalog na pritisak dugmeta E igrica treba da se nastavi i njeno stanje postaje RUNNING"){

        Game* g;
        game_state result = RUNNING;

        g = Game::instance();
        g->setCurrentState(DIALOGUE);
        g->keyPressEvent(new QKeyEvent (QEvent::KeyPress,Qt::Key_E,Qt::NoModifier,"e"));

        REQUIRE(result == g->getCurrentState());

    }

    SECTION("Ako smo u igri i pritisnemo Escape dugme, game treba da udje u stanje PAUSE"){

        Game* g;
        game_state result = PAUSE;

        g = Game::instance();
        g->setCurrentState(RUNNING);
        g->keyPressEvent(new QKeyEvent (QEvent::KeyPress,Qt::Key_Escape,Qt::NoModifier,"e"));

        REQUIRE(result == g->getCurrentState());

    }

    SECTION("Ako je igra zavrsena stanje treba da bude GAME_OVER"){

        Game* g;
        game_state result = GAME_OVER;

        g = Game::instance();
        g->setCurrentState(RUNNING);
        g->end_game();

        REQUIRE(result == g->getCurrentState());

    }

    SECTION("Ako se pokrene pause_music(), musicPaused treba da se postavi na true"){

        Game* g;
        bool result = true;

        g = Game::instance();
        g->musicPaused = false;

        g->pause_music();

        REQUIRE(g->musicPaused == result);

    }

    SECTION("Ako se pokrene open_menu(), musicPaused treba da se postavi na false ako je stanje igre bilo GAME_OVER"){

        Game* g;
        bool result = false;

        g = Game::instance();
        g->setCurrentState(GAME_OVER);

        g->open_menu();

        REQUIRE(g->musicPaused == result);

    }

    SECTION("Ako se pokrene open_menu(), musicPaused treba da se postavi na false ako je stanje igre bilo PAUSE"){

        Game* g;
        bool result = false;

        g = Game::instance();
        g->setCurrentState(PAUSE);

        g->open_menu();

        REQUIRE(g->musicPaused == result);

    }

}
