#include <catch2/catch.hpp>
#include "../MrSnowman/code/enemy_show.h"
#include "../MrSnowman/code/player_show.h"
#include "../MrSnowman/code/snowballammo_show.h"

TEST_CASE("Enemy_show","[enemy], [show]") {
    SECTION("Ako je pozvana funkcija setImg za neprijatelja tipa Wolf, uspesno je postavljena tekstura"){

        Enemy_show* e = new Enemy_show(Wolf,10,15);

        e->setImg();

        REQUIRE_NOTHROW(e->pixmap().isNull());

    }

    SECTION("Ako je pozvana funkcija setImg za neprijatelja tipa Penguin, uspesno je postavljena tekstura"){

        Enemy_show* e = new Enemy_show(Penguin,10,15);

        e->setImg();

        REQUIRE_NOTHROW(e->pixmap().isNull());

    }

    SECTION("Ako se igrac sudari sa neprijateljem, health mu se smanjuje za 1"){

        Enemy_show* e = new Enemy_show(Wolf,10,15);
        Player_show* p = new Player_show(10,15);
        unsigned damage = 1;
        unsigned result = p->getPlayerInfo()->getHP() - damage;

        e->collidingWithPlayer(p);

        REQUIRE(result == p->getPlayerInfo()->getHP());

    }


    SECTION("Ako igrac ima imunity i sudari se sa neprijateljem, health mu ostaje isti"){

        Enemy_show* e = new Enemy_show(Wolf,10,15);
        Player_show* p = new Player_show(10,15);
        unsigned result = p->getPlayerInfo()->getHP();

        p->getPlayerInfo()->makeImune();
        e->collidingWithPlayer(p);

        REQUIRE(result == p->getPlayerInfo()->getHP());

    }

    SECTION("Ako se sudare grudva i neprijatelj, neprijatelj se uklanja sa scene"){

        Enemy_show* e = new Enemy_show(Wolf,10,15);
        SnowballAmmo_show* s = new SnowballAmmo_show(10,15, Direction::RIGHT);
        bool result = true;

        e->collidingWithSnowball(s);

        REQUIRE(result == e->getToBeRemoved());

    }
}
