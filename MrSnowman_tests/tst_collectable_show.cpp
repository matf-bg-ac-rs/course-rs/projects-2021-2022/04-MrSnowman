#include <catch2/catch.hpp>

#include "../MrSnowman/code/collectable_show.h"

TEST_CASE("Collectable_show", "[class]") {

    SECTION("Konstruktor Collectable_show, uspešno konstruiše nasumičan objekat tipa SNOWBALL") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::SNOWBALL;

        // Act + Assert
        REQUIRE_NOTHROW(Collectable_show(type, x, y));

    }

    SECTION("Konstruktor Collectable_show, uspešno konstruiše nasumičan objekat tipa CARROT") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::CARROT;

        // Act + Assert
        REQUIRE_NOTHROW(Collectable_show(type, x, y));

    }

    SECTION("Ako Player_show ima kontakt sa Collectable_show tipa CARROT, i IMA maksimalnu dozvoljenu vrednost za atribut hp,"
            " vrednost atribut se NECE POVECATI za 1 ") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::CARROT;
        Collectable_show* cs = new Collectable_show(type, x, y);

        Player_show* ps = new Player_show(x, y);
        const auto oldHp = ps->getPlayerInfo()->getHP();

        // Act
        cs->collidingWithPlayer(ps);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getHP() == oldHp);

        delete cs;
        delete ps;


    }

    SECTION("Ako Player_show ima kontakt sa Collectable_show tipa CARROT, i NEMA maksimalnu dozvoljenu vrednost za atribut hp, "
            "vrednost tog atributa CE SE POVECATI za 1") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::CARROT;
        Collectable_show* cs = new Collectable_show(type, x, y);

        Player_show* ps = new Player_show(x, y);
        ps->getPlayerInfo()->takeDamage(1);
        const int HPBeforeCollision = ps->getPlayerInfo()->getHP();

        // Act
        cs->collidingWithPlayer(ps);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getHP() == HPBeforeCollision + 1);

        delete cs;
        delete ps;

    }


    SECTION("Ako Player_show ima kontakt sa Collectable_show tipa SNOWBALL, i IMA maksimalnu dozvoljenu vrednost za atribut ammo,"
            " vrednost atribut se NECE POVECATI za 1 ") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::SNOWBALL;
        Collectable_show* cs = new Collectable_show(type, x, y);

        Player_show* ps = new Player_show(x, y);

        const auto oldAmmo = ps->getPlayerInfo()->getAmmo();

        // Act
        cs->collidingWithPlayer(ps);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getAmmo() == oldAmmo);

        delete cs;
        delete ps;


    }


    SECTION("Ako Player_show ima kontakt sa Collectable_show tipa SNOWBALL, i NEMA maksimalnu dozvoljenu vrednost za atribut ammo, "
            "vrednost tog atributa CE SE POVECATI za 1") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::SNOWBALL;
        Collectable_show* cs = new Collectable_show(type, x, y);

        Player_show* ps = new Player_show(x, y);
        ps->getPlayerInfo()->throwASnowball();
        const int NumberOfSnowballBeforeCollision = ps->getPlayerInfo()->getAmmo();

        // Act
        cs->collidingWithPlayer(ps);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getAmmo() == NumberOfSnowballBeforeCollision + 1);

        delete cs;
        delete ps;

    }


    SECTION("Ako Player_show ima kontakt sa Collectable_show tipa CARROT (i Player nema vec max hp), "
            "atribut toBeRemoved treba da bude TRUE") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::CARROT;
        Collectable_show* cs = new Collectable_show(type, x, y);

        Player_show* ps = new Player_show(x, y);
        ps->getPlayerInfo()->takeDamage(1);

        // Act
        cs->collidingWithPlayer(ps);

        // Assert
        REQUIRE(cs->getToBeRemoved() == true);

        delete cs;
        delete ps;


    }


    SECTION("Ako Player_show ima kontakt sa Collectable_show tipa SNOWBALL (i Player nema vec max ammo), "
            "atribut toBeRemoved treba da bude TRUE") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::SNOWBALL;
        Collectable_show* cs = new Collectable_show(type, x, y);

        Player_show* ps = new Player_show(x, y);
        ps->getPlayerInfo()->throwASnowball();

        // Act
        cs->collidingWithPlayer(ps);

        // Assert
        REQUIRE(cs->getToBeRemoved() == true);

        delete cs;
        delete ps;


    }

    SECTION("Ako Player_show ima kontakt sa Collectable_show tipa CARROT (i Player vec ima max hp), "
            "atribut toBeRemoved treba da bude FALSE") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::CARROT;
        Collectable_show* cs = new Collectable_show(type, x, y);

        Player_show* ps = new Player_show(x, y);

        // Act
        cs->collidingWithPlayer(ps);

        // Assert
        REQUIRE(cs->getToBeRemoved() == false);

        delete cs;
        delete ps;


    }

    SECTION("Ako Player_show ima kontakt sa Collectable_show tipa SNOWBALL (i Player vec ima max ammo), "
            "atribut toBeRemoved treba da bude FALSE") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        const Collectable_type type = Collectable_type::SNOWBALL;
        Collectable_show* cs = new Collectable_show(type, x, y);

        Player_show* ps = new Player_show(x, y);

        // Act
        cs->collidingWithPlayer(ps);

        // Assert
        REQUIRE(cs->getToBeRemoved() == false);

        delete cs;
        delete ps;


    }

}
