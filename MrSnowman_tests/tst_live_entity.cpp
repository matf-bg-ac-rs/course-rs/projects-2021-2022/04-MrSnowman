#include <catch2/catch.hpp>
#include "../MrSnowman/code/live_entity.h"


TEST_CASE("Live_entity","[live_entity]") {
    SECTION("Ako live_entity primi 1 udarac, a ima vise od 0 HP, treba da mu se smanji HP za 1"){

        Live_entity* e = new Live_entity(0,0,3);
        unsigned result = e->getHP() - 1;

        e->takeDamage(1);

        REQUIRE(result == e->getHP());
    }
    SECTION("Ako live_entity primi 1 udarac, a ima 0 HP, treba da mu HP ostane 0"){

        Live_entity* e = new Live_entity(0,0,0);
        unsigned result = 0;

        e->takeDamage(1);

        REQUIRE(result == e->getHP());
    }
    SECTION("Ako live_entity primi udarac koji ga supsta na 0 HP, isAlive treba da mu se postavi na false, pa checkDeath treba da vrati true"){

        Live_entity* e = new Live_entity(0,0,1);
        bool result = true;

        e->takeDamage(1);

        REQUIRE(result == e->checkDeath());
    }
}
