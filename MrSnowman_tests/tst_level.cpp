#include <catch2/catch.hpp>
#include "../MrSnowman/code/level.h"
#include "../MrSnowman/code/player_show.h"
#include "../MrSnowman/code/tile_show.h"
#include "../MrSnowman/code/enemy_show.h"
#include "../MrSnowman/code/collectable_show.h"


TEST_CASE("Level","[level]") {

    SECTION("Proverava da li radi loadlevel ako posaljemo dobre podatke(dobar id nivoa (1-3))"){

        Level* l = new Level();
        QGraphicsScene *scene = new QGraphicsScene();

        Player_show* p = l->loadLevel(2,scene);

        REQUIRE_NOTHROW(p == nullptr);

        delete p;
        delete scene;
        delete l;

    }

    SECTION("Proverava da li radi loadlevel ako posaljemo lose podatke(los id nivoa (x<1 ili x>3))"){

        Level* l = new Level();
        QGraphicsScene *scene = new QGraphicsScene();

        Player_show* p = l->loadLevel(7,scene);

        REQUIRE(p == nullptr);

        delete p;
        delete scene;
        delete l;

    }

    SECTION("Proverava da li se dobro ucitavaju platforme ako posaljemo dobre podatke(dobar id nivoa (1-3)), ocekujemo vise od 0"){

        Level* l = new Level();
        QGraphicsScene *scene = new QGraphicsScene();
        unsigned result = 0;

        Player_show* p = l->loadLevel(1,scene);
        const QList<Tile_show *>& t = l->getTiles();

        REQUIRE(t.length() > result);

        delete p;
        delete scene;
        delete l;

    }
    SECTION("Proverava da li se dobro ucitavaju platforme ako posaljemo lose podatke(los id nivoa (x<1 ili x>3)), ocekujemo da ima 0"){

        Level* l = new Level();
        QGraphicsScene *scene = new QGraphicsScene();
        unsigned result = 0;

        Player_show* p = l->loadLevel(5,scene);
        const QList<Tile_show *>& t = l->getTiles();

        REQUIRE(t.length() == result);

        delete p;
        delete scene;
        delete l;

    }
    SECTION("Proverava da li se dobro ucitavaju neprijatelji ako posaljemo dobre podatke(dobar id nivoa (1-3)), ocekujemo vise od 0"){

        Level* l = new Level();
        QGraphicsScene *scene = new QGraphicsScene();
        unsigned result = 0;

        Player_show* p = l->loadLevel(1,scene);
        QList<Enemy_show *>& e = l->getEnemies();

        REQUIRE(e.length() > result);

        delete p;
        delete scene;
        delete l;


    }
    SECTION("Proverava da li se dobro ucitavaju neprijatelji ako posaljemo lose podatke(los id nivoa (x<1 ili x>3)), ocekujemo da ima 0"){

        Level* l = new Level();
        QGraphicsScene *scene = new QGraphicsScene();
        unsigned result = 0;

        Player_show* p = l->loadLevel(5,scene);
        QList<Enemy_show *>& e = l->getEnemies();

        REQUIRE(e.length() == result);

        delete p;
        delete scene;
        delete l;

    }
    SECTION("Proverava da li se dobro ucitavaju objekti za sakupljanje ako posaljemo dobre podatke(dobar id nivoa (1-3)), ocekujemo vise od 0"){

        Level* l = new Level();
        QGraphicsScene *scene = new QGraphicsScene();
        unsigned result = 0;

        Player_show* p = l->loadLevel(1,scene);
        QList<Collectable_show *>& c = l->getCollectables();

        REQUIRE(c.length() > result);

        delete p;
        delete scene;
        delete l;


    }
    SECTION("Proverava da li se dobro ucitavaju objekti za sakupljanje ako posaljemo lose podatke(los id nivoa (x<1 ili x>3)), ocekujemo da ima 0"){

        Level* l = new Level();
        QGraphicsScene *scene = new QGraphicsScene();
        unsigned result = 0;

        Player_show* p = l->loadLevel(5,scene);
        QList<Collectable_show *>& c = l->getCollectables();

        REQUIRE(c.length() == result);

        delete p;
        delete scene;
        delete l;

    }

}
