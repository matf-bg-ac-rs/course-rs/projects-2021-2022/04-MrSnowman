#include <catch2/catch.hpp>
#include "../MrSnowman/code/health_bar.h"


TEST_CASE("Health_bar","[health]") {
    SECTION("Proverava da li postavlja teksturu za hp ako se posalje dobar broj (0-3) hp-a"){

        Health_bar* h = new Health_bar();

        h->setHealth(1);

        REQUIRE_NOTHROW(h->pixmap().isNull());

        delete h;

    }

    SECTION("Proverava da li NE postavlja teksturu za hp ako se posalje los broj (x<0 ili x>3) hp-a"){

        Health_bar* h = new Health_bar();

        h->setHealth(4);

        REQUIRE(h->pixmap().isNull());

        delete h;

    }
}
