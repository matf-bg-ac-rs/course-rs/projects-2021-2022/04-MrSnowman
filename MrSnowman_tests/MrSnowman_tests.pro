TEMPLATE = app
QT += gui \
      multimedia
QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=$$(CATCH_INCLUDE_DIR)
# set by Qt Creator wizard
isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR="./catch2"
!isEmpty(CATCH_INCLUDE_DIR): INCLUDEPATH *= $${CATCH_INCLUDE_DIR}

isEmpty(CATCH_INCLUDE_DIR): {
    message("CATCH_INCLUDE_DIR is not set, assuming Catch2 can be found automatically in your system")
}

SOURCES +=     main.cpp   \
               tst_collectable_show.cpp \
               tst_enemy.cpp \
               tst_enemy_show.cpp \
               tst_game.cpp \
               ../MrSnowman/code/live_entity.cpp \
               ../MrSnowman/code/entity.cpp \
               ../MrSnowman/code/object.cpp \
               ../MrSnowman/code/button.cpp \
               ../MrSnowman/code/collectable.cpp \
               ../MrSnowman/code/collectable_show.cpp \
               ../MrSnowman/code/collidable.cpp \
               ../MrSnowman/code/enemy.cpp \
               ../MrSnowman/code/enemy_show.cpp \
               ../MrSnowman/code/game.cpp \
               ../MrSnowman/code/health_bar.cpp \
               ../MrSnowman/code/health_text.cpp \
               ../MrSnowman/code/level.cpp \
               ../MrSnowman/code/menu.cpp \
               ../MrSnowman/code/player.cpp \
               ../MrSnowman/code/player_show.cpp \
               ../MrSnowman/code/settings.cpp \
               ../MrSnowman/code/show.cpp \
               ../MrSnowman/code/snowball_text.cpp \
               ../MrSnowman/code/snowballammo.cpp \
               ../MrSnowman/code/snowballammo_show.cpp \
               ../MrSnowman/code/tile.cpp \
               ../MrSnowman/code/tile_show.cpp \
               tst_health_bar.cpp \
               tst_level.cpp \
               tst_live_entity.cpp \
               tst_player.cpp \
               tst_player_show.cpp \
               tst_snowballammo.cpp \
               tst_tile_show.cpp

HEADERS +=     ../MrSnowman/code/live_entity.h  \
               ../MrSnowman/code/entity.h \
               ../MrSnowman/code/object.h \
               ../MrSnowman/code/button.h \
               ../MrSnowman/code/collectable.h \
               ../MrSnowman/code/collectable_show.h \
               ../MrSnowman/code/collidable.h \
               ../MrSnowman/code/enemy.h \
               ../MrSnowman/code/enemy_show.h \
               ../MrSnowman/code/game.h \
               ../MrSnowman/code/health_bar.h \
               ../MrSnowman/code/health_text.h \
               ../MrSnowman/code/level.h \
               ../MrSnowman/code/menu.h \
               ../MrSnowman/code/player.h \
               ../MrSnowman/code/player_show.h \
               ../MrSnowman/code/settings.h \
               ../MrSnowman/code/show.h \
               ../MrSnowman/code/snowball_text.h \
               ../MrSnowman/code/snowballammo.h \
               ../MrSnowman/code/snowballammo_show.h \
               ../MrSnowman/code/tile.h \
               ../MrSnowman/code/tile_show.h



RESOURCES += \
               ../MrSnowman/images.qrc \
               ../MrSnowman/sound.qrc \
               ../MrSnowman/txt.qrc

DISTFILES += \
               ../MrSnowman/code/resources/img/background.jpg \
               ../MrSnowman/code/resources/img/background_old.jpg \
               ../MrSnowman/code/resources/img/carrot.png \
               ../MrSnowman/code/resources/img/credits.jpg \
               ../MrSnowman/code/resources/img/credits2.jpg \
               ../MrSnowman/code/resources/img/cursor.png \
               ../MrSnowman/code/resources/img/dialog_1.png \
               ../MrSnowman/code/resources/img/dialog_2.png \
               ../MrSnowman/code/resources/img/dialog_3.png \
               ../MrSnowman/code/resources/img/dialog_template.png \
               ../MrSnowman/code/resources/img/icon.png \
               ../MrSnowman/code/resources/img/loadingscreen.jpg \
               ../MrSnowman/code/resources/img/mainmenu.jpg \
               ../MrSnowman/code/resources/img/mainmenu_button.jpg \
               ../MrSnowman/code/resources/img/mainmenu_button.png \
               ../MrSnowman/code/resources/img/penguin_standing.png \
               ../MrSnowman/code/resources/img/penguin_standing2.png \
               ../MrSnowman/code/resources/img/penguin_walk1.png \
               ../MrSnowman/code/resources/img/penguin_walk2.png \
               ../MrSnowman/code/resources/img/settings_button.jpg \
               ../MrSnowman/code/resources/img/settings_button_check_off.png \
               ../MrSnowman/code/resources/img/settings_button_check_on.png \
               ../MrSnowman/code/resources/img/settings_button_off.png \
               ../MrSnowman/code/resources/img/settings_button_on.png \
               ../MrSnowman/code/resources/img/snow_tile.png \
               ../MrSnowman/code/resources/img/snowball.png \
               ../MrSnowman/code/resources/img/snowball_old.png \
               ../MrSnowman/code/resources/img/snowman.png \
               ../MrSnowman/code/resources/img/snowman_old.png \
               ../MrSnowman/code/resources/img/sound_button_off.png \
               ../MrSnowman/code/resources/img/sound_button_on.png \
               ../MrSnowman/code/resources/img/tile.png \
               ../MrSnowman/code/resources/img/wolf_standing.png \
               ../MrSnowman/code/resources/img/wolf_walk1.png

