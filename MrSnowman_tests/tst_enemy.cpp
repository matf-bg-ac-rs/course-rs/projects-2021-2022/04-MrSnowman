#include <catch2/catch.hpp>
#include "../MrSnowman/code/enemy.h"

TEST_CASE("Enemy testing", "[enemy]") {
    SECTION("Ako je neprijatelj presao makimalnu distancu, menja mu se smer kretanja"){

        Enemy* e = new Enemy(Wolf,0,0);
        Direction result = Direction::LEFT;

        while(e->getMovingDirection() == Direction::RIGHT){
            e->move();
        }

        REQUIRE(result == e->getMovingDirection());

    }

    SECTION("Ako je smer kretanja neprijatelja levo, pozicija mu se umanjila za SPEED"){

        Enemy* e = new Enemy(Wolf,10,15);
        unsigned result = e->getX() + e->getMaxDistance() - e->getSpeed();

        while(e->getMovingDirection() == Direction::RIGHT)
            e->move();
        e->move();

        REQUIRE(result == e->getX());

    }

    SECTION("Ako je smer kretenja neprijatelja desno, pozicija mu se povecala za SPEED"){

        Enemy* e = new Enemy(Wolf,10,15);
        unsigned result = e->getX() + e->getSpeed();

        e->move();

        REQUIRE(result == e->getX());

    }

    SECTION("Ako je napravljne objekat Enemy, postavljena mu je predjena distanca na 0"){

        double result = 0.0;

        Enemy* e = new Enemy(Wolf, 10, 15);

        REQUIRE(result == e->getDistancePassed());

    }

    SECTION("Ako je napravljen vuk, postavljen mu je tip na Wolf"){

        Enemy_type result = Wolf;

        Enemy* e = new Enemy(Wolf, 10, 15);

        REQUIRE(result == e->getEnemyType());

    }

    SECTION("Ako je napravljen pingvin, postavljen mu je tip na Penguin"){

        Enemy_type result = Penguin;

        Enemy* e = new Enemy(Penguin, 10, 15);

        REQUIRE(result == e->getEnemyType());

    }
}


