#include <catch2/catch.hpp>
#include "../MrSnowman/code/snowballammo.h"
#include "../MrSnowman/code/entity.h"


TEST_CASE("Snowball ammo","[ammo]") {
    SECTION("Ako je projektil(grudva) napravljena da se krece DESNO, pozicija na X osi posle pozivanja move() bi trebalo da bude veca od pocetne"){

        SnowballAmmo* h = new SnowballAmmo(0, 0, Direction::RIGHT);
        double result = h->getX();

        h->move();

        REQUIRE(h->getX() > result);

        delete h;

    }

    SECTION("Ako je projektil(grudva) napravljena da se krece LEVO, pozicija na X osi posle pozivanja move() bi trebalo da bude manja od pocetne"){

        SnowballAmmo* h = new SnowballAmmo(0, 0, Direction::LEFT);
        double result = h->getX();

        h->move();

        REQUIRE(h->getX() < result);

        delete h;

    }
}
