#include <catch2/catch.hpp>
#include "../MrSnowman/code/player.h"


TEST_CASE("Player","[player]") {
    SECTION("Ako igrac ima maksimum municije, onda ne treba da mu se poveca broj municije pozivom increaseAmmo()"){

        Player* p = new Player(0,0,3);
        unsigned result = p->getAmmo();

        p->increaseAmmo();

        REQUIRE(result == p->getAmmo());
    }
    SECTION("Ako igrac ima manje od maksimuma municije, onda treba da mu se poveca broj municije pozivom increaseAmmo()"){

        Player* p = new Player(0,0,3);
        p->throwASnowball();
        unsigned result = p->getAmmo()+1;

        p->increaseAmmo();

        REQUIRE(result == p->getAmmo());
    }
    SECTION("Ako igrac ima 0 municije, onda ne treba da mu se smanji broj kad se pozove throwASnowball()"){

        Player* p = new Player(0,0,3);
        p->throwASnowball();
        p->throwASnowball();
        p->throwASnowball();
        p->throwASnowball();
        p->throwASnowball();
        unsigned result = 0;

        p->throwASnowball();

        REQUIRE(result == p->getAmmo());
    }
    SECTION("Ako igrac ima vise 0 municije, onda treba da mu se smanji broj kad se pozove throwASnowball()"){

        Player* p = new Player(0,0,3);
        unsigned result = PLAYER_MAX_AMMO-1;

        p->throwASnowball();

        REQUIRE(result == p->getAmmo());
    }
    SECTION("Ako igrac ima manje od PLAYER_MAX_HP HP-a, increaseHealth treba da mu poveca HP za 1"){

        Player* p = new Player(0,0,1);
        unsigned result = p->getHP()+1;

        p->increaseHealth();

        REQUIRE(result == p->getHP());
    }
    SECTION("Ako igrac ima PLAYER_MAX_HP HP-a, increaseHealth treba da mu ostavi HP na istom"){

        Player* p = new Player(0,0,PLAYER_MAX_HP);
        unsigned result = PLAYER_MAX_HP;

        p->increaseHealth();

        REQUIRE(result == p->getHP());
    }
    SECTION("Funkcija changePosition treba da postavi igracevu x koordinatu na trazenu vrednosti"){

        Player* p = new Player(0,0,PLAYER_MAX_HP);
        int result = 10;

        p->changePosition(10,5);

        REQUIRE(result == p->getX());
    }
    SECTION("Funkcija changePosition treba da postavi igracevu y koordinatu na trazenu vrednosti"){

        Player* p = new Player(0,0,PLAYER_MAX_HP);
        int result = 10;

        p->changePosition(5,10);

        REQUIRE(result == p->getY());
    }
    SECTION("Ako igrac skoci, treba da mu se postavi isFalling na true"){

        Player* p = new Player(0,0,3);
        bool result = true;

        p->jump();

        REQUIRE(result == p->getIsFalling());
    }
    SECTION("Ako igrac skoci, treba da mu se postavi jumpDuration na PLAYER_JUMP_DURATION"){

        Player* p = new Player(0,0,3);
        double result = PLAYER_JUMP_DURATION;

        p->jump();

        REQUIRE(result == p->getJumpDuration());
    }
    SECTION("Ako se racuna vertikalna brzina posle skoka, ona treba da bude PLAYER_JUMP_SPEED*PLAYER_JUMP_DURATION"){

        Player* p = new Player(0,0,3);
        double result = PLAYER_JUMP_SPEED*PLAYER_JUMP_DURATION;

        p->jump();
        p->calculateVerticalSpeed();

        REQUIRE(result == p->getVerticalSpeed());
    }
    SECTION("Nakon racunanja vertikalne brzine jumpDuration treba da se smanji za PLAYER_FALLING_SPEED"){

        Player* p = new Player(0,0,3);
        double result = p->getJumpDuration()-PLAYER_FALLING_SPEED;

        p->calculateVerticalSpeed();

        REQUIRE(result == p->getJumpDuration());
    }
    SECTION("Nakon stopFalling vertikalna brzina igraca treba da bude 0"){

        Player* p = new Player(0,0,3);
        double result = 0.0f;

        p->stopFalling();

        REQUIRE(result == p->getVerticalSpeed());
    }
    SECTION("Nakon stopFalling jumpDuration igraca treba da bude 0"){

        Player* p = new Player(0,0,3);
        double result = 0.0f;

        p->stopFalling();

        REQUIRE(result == p->getJumpDuration());
    }
    SECTION("Nakon stopFalling isFalling promenljiva igraca treba da bude false"){

        Player* p = new Player(0,0,3);
        bool result = false;

        p->stopFalling();

        REQUIRE(result == p->getIsFalling());
    }

    SECTION("Ako se player treba da pocne kretanje poziva se startMoving(), postavlaj se isMoving na true"){

        Player* p = new Player(0,0,3);
        bool result = true;

        p->startMoving();

        REQUIRE(result == p->getIsMoving());
    }
    SECTION("Ako se player zaustavi poziva se stopMoving(), postavlaj se isMoving na false"){

        Player* p = new Player(0,0,3);
        bool result = false;

        p->stopMoving();

        REQUIRE(result == p->getIsMoving());
    }

    SECTION("Ako se pozove funkcija move, a igracev isMoving je postavljen na false, onda ne treba da mu se menja x pozicija"){

        Player* p = new Player(0,0,3);
        double result = p->getX();

        p->move();

        REQUIRE(result == p->getX());
    }
    SECTION("Ako se igrac krece u smeru Direction::RIGHT treba da mu se poveca x pozicija za PLAYER_SPEED"){

        Player* p = new Player(0,0,3);
        double result = p->getX()+p->getSpeed();
        p->startMoving();

        p->move();

        REQUIRE(result == p->getX());
    }
    SECTION("Ako se igrac krece u smeru Direction::LEFT, a to bi mu x koordinatu postavilo na negativan broj, onda x = 0"){

        Player* p = new Player(0,0,3);
        double result = 0.0f;
        p->startMoving();

        p->changeDirection(Direction::LEFT);
        p->move();

        REQUIRE(result == p->getX());
    }
    SECTION("Ako se igrac krece u smeru Direction::LEFT, a x koordinata ostaje pozitivna, onda x treba da se smanji za PLAYER_SPEED"){

        Player* p = new Player(20,0,3);
        double result = p->getX()-p->getSpeed();
        p->startMoving();

        p->changeDirection(Direction::LEFT);
        p->move();

        REQUIRE(result == p->getX());
    }
    SECTION("Ako je igracev isFalling false, ne treba da mu se menja y koordinata"){

        Player* p = new Player(0,0,3);
        double result = p->getY();

        p->move();

        REQUIRE(result == p->getY());
    }
    SECTION("Ako je igracev isFalling true, treba da se racuna vertikalna brzina (PLAYER_JUMP_SPEED*getJumpDuration()) i za toliko smanji y koordinata"){

        Player* p = new Player(0,0,3);
        double result = p->getY()-PLAYER_JUMP_SPEED*p->getJumpDuration();
        p->startFalling();

        p->move();

        REQUIRE(result == p->getY());
    }
    SECTION("Nakon igraceve move funkcije, shouldFall treba da bude true"){

        Player* p = new Player(0,0,3);
        bool result = true;

        p->move();

        REQUIRE(result == p->getShouldFall());
    }
    SECTION("Nakon igraceve move funkcije, ako je shouldFall true, onda isFalling treba da se postavi na true"){

        Player* p = new Player(0,0,3);
        bool result = true;

        p->prepareToFall();
        p->move();

        REQUIRE(result == p->getIsFalling());
    }
    SECTION("Funkcija prepareToFall treba da postavi shouldFall na true"){

        Player* p = new Player(0,0,3);
        bool result = true;

        p->prepareToFall();

        REQUIRE(result == p->getShouldFall());
    }
    SECTION("Funkcija unprepareToFall treba da postavi shouldFall na false"){

        Player* p = new Player(0,0,3);
        bool result = false;

        p->unprepareToFall();

        REQUIRE(result == p->getShouldFall());
    }

    SECTION("Ako se pozove funkcija makeImune() treba da player postane imun na IMUNITY_DURATION"){

        Player* p = new Player(0,0,3);
        unsigned result = IMUNITY_DURATION;

        p->makeImune();

        REQUIRE(p->getImunityDuration() == result);
    }

    SECTION("Ako se pozove funkcija decreaseImunity() treba da se smanji imunityDuration za 1"){

        Player* p = new Player(0,0,3);
        unsigned result = IMUNITY_DURATION-1;

        p->makeImune();
        p->decreaseImunity();

        REQUIRE(p->getImunityDuration() == result);
    }
    SECTION("Ako se pozove funkcija decreaseImunity(), a imunityDuration je 0 onda ne treba da se smanji"){

        Player* p = new Player(0,0,3);
        unsigned result = 0;

        p->decreaseImunity();

        REQUIRE(p->getImunityDuration() == result);
    }


}
