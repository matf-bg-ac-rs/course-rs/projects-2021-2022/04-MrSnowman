#include <catch2/catch.hpp>
#include "../MrSnowman/code/tile_show.h"
#include "../MrSnowman/code/snowballammo_show.h"
#include "../MrSnowman/code/player_show.h"


TEST_CASE("Tile_show","[tile], [show]") {

    // DIVNA TEST

    SECTION("Ako se grudva sudara sa platformom kada se krece desno, onda se grudva uklanja sa scene"){

        Tile_show* ts = new Tile_show(":/resources/img/tile.png",10, 15);
        SnowballAmmo_show* s = new SnowballAmmo_show(10,15, Direction::RIGHT);
        bool result = true;

        ts->collidingWithSnowball(s);

        REQUIRE(result == s->getToBeRemoved());

    }

    SECTION("Ako se igrac sudario ili pao na ledenice, onda njegovo stanje treba da bude nije ziv"){
        Tile_show* ts = new Tile_show(":/resources/img/icicle.png",10, 15, true, true);
        Player_show* ps = new Player_show(10,15);
        bool result = true;

        ts->collidingWithPlayer(ps);

        REQUIRE(result == ps->getPlayerInfo()->checkDeath());

    }

    SECTION("Ako se igrac sudario ili pao na ledenice, onda je njegov health na 0"){
        Tile_show* ts = new Tile_show(":/resources/img/icicle.png",10, 15, true, true);
        Player_show* ps = new Player_show(10,15);
        unsigned result = 0;

        ts->collidingWithPlayer(ps);

        REQUIRE(result == ps->getPlayerInfo()->getHP());

    }

    SECTION("Ako je igrac skocio na platformu, onda prekida da pada i vrednost isFalling mu postaje false"){
        Tile_show* ts = new Tile_show(":/resources/img/tile.png",10, 110);
        Player_show* ps = new Player_show(10 ,59);
        bool result = false;

        ps->getPlayerInfo()->startFalling();
        ts->collidingWithPlayer(ps);

        REQUIRE(result == ps->getPlayerInfo()->getIsFalling());

    }

    SECTION("Ako je igrac skocio na platformu, onda prekida da pada i njegova vertikalna brzina je 0"){
        Tile_show* ts = new Tile_show(":/resources/img/tile.png",10, 110);
        Player_show* ps = new Player_show(10 ,59);
        double result = 0.0;

        ps->getPlayerInfo()->startFalling();
        ts->collidingWithPlayer(ps);

        REQUIRE(result == ps->getPlayerInfo()->getVerticalSpeed());

    }

    // VUKAN TEST


    SECTION("Konstruktor tile_show, uspešno konstruiše nasumičan objekat"){

        // Arrange

        const double x = 0.0;
        const double y = 0.0;

        // Act + Assert
        REQUIRE_NOTHROW(Tile_show(":/resources/img/snow_tile.png", x, y));

    }

    SECTION("Konstruktor tile_show, ako mu je data putanja ka teksturi koja ne postoji, baca exception"){

        // Arrange

        const double x = 0.0;
        const double y = 0.0;

        // Act + Assert
        REQUIRE(Tile_show("ovaPutanjaNePostoji", x, y).pixmap().isNull());

    }

    SECTION("Kada Player udari platformu sa LEVE strane, onda se atribut isMoving unutar Playera postavlja na FALSE"){

        // Arrange

        const double x = 10.0;
        const double y = 0.0;
        const QString name = ":/resources/img/snow_tile.png";
        Tile_show* ts = new Tile_show(name, x, y);

        const double xp = 0.0;
        const double yp = 0.0;
        Player_show* ps = new Player_show(xp, yp);
        ps->getPlayerInfo()->changeDirection(Direction::RIGHT);
        ps->getPlayerInfo()->changeIsMoving(true);

        // Act
        ts->collidingWithPlayer(ps);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getIsMoving() == false);

        delete ts;
        delete ps;

    }


    SECTION("Kada Player udari platformu sa DESNE strane, onda se atribut isMoving unutar Playera postavlja na FALSE"){

        // Arrange

        const double x = 0.0;
        const double y = 0.0;
        const QString name = ":/resources/img/snow_tile.png";
        Tile_show* ts = new Tile_show(name, x, y);

        const double xp = 10.0;
        const double yp = 0.0;
        Player_show* ps = new Player_show(xp, yp);
        ps->getPlayerInfo()->changeDirection(Direction::LEFT);
        ps->getPlayerInfo()->changeIsMoving(true);

        // Act
        ts->collidingWithPlayer(ps);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getIsMoving() == false);

        delete ts;
        delete ps;

    }


    SECTION("Kada Player udari platformu sa GORNJE strane (Player udara sa gornje strane), "
            "onda se atribut isMoving unutar Playera postavlja na TRUE"){


        // Arrange

        const double x = 0.0;
        const double y = 0.0;
        const QString name = ":/resources/img/snow_tile.png";
        Tile_show* ts = new Tile_show(name, x, y);

        const double xp = 0.0;
        const double yp = 20.0;
        Player_show* ps = new Player_show(xp, yp);

        // Act
        ts->collidingWithPlayer(ps);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getIsFalling() == true);

        delete ts;
        delete ps;

    }

    SECTION("Kada Player udari platformu sa DONJE strane (kada Player udara sa DONJE strane), "
            "onda se atribut jumpDuration unutar Playera postavlja na 0.0"){

        // Arrange
        const double x = 0.0;
        const double y = 60.0;
        const QString name = ":/resources/img/snow_tile.png";
        Tile_show* ts = new Tile_show(name, x, y);

        const double xp = 0.0;
        const double yp = 0.0;
        Player_show* ps = new Player_show(xp, yp);

        // Act
        ts->collidingWithPlayer(ps);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getJumpDuration() == 0.0);

        delete ts;
        delete ps;

    }

}
