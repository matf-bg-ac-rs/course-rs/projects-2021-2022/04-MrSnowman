#include <catch2/catch.hpp>

#include "../MrSnowman/code/player_show.h"

TEST_CASE("Player_show", "[class]") {

    SECTION("Konstruktor Player_snow uspesno konstruise nasumican objekat") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;

        // Act + Assert
        REQUIRE_NOTHROW(Player_show(x, y));

    }

    SECTION("Kada je pritisnuta leva strelica, trebalo bi da se menja atribut dir unutar Playera na LEFT") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        Player_show* ps = new Player_show(x, y);

        QKeyEvent* ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Left, Qt::NoModifier);

        // Act
        ps->keyPressEvent(ev);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getMovingDirection() == Direction::LEFT);


        delete ps;
        delete ev;

    }

    SECTION("Kada je pritisnuta leva strelica, trebalo bi da se menja atribut isMoving unutar Playera na TRUE") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        Player_show* ps = new Player_show(x, y);

        QKeyEvent* ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Left, Qt::NoModifier);

        // Act
        ps->keyPressEvent(ev);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getIsMoving() == true);


        delete ps;
        delete ev;

    }


    SECTION("Kada je pritisnuta desna strelica, trebalo bi da se menja atribut dir unutar Playera na RIGHT") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        Player_show* ps = new Player_show(x, y);

        QKeyEvent* ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Right, Qt::NoModifier);

        // Act
        ps->keyPressEvent(ev);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getMovingDirection() == Direction::RIGHT);


        delete ps;
        delete ev;

    }

    SECTION("Kada je pritisnuta desna strelica, trebalo bi da se menja atribut isMoving unutar Playera na TRUE") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        Player_show* ps = new Player_show(x, y);

        QKeyEvent* ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Right, Qt::NoModifier);

        // Act
        ps->keyPressEvent(ev);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getIsMoving() == true);


        delete ps;
        delete ev;

    }


    SECTION("Kada je pritisnut spacebar, trebalo bi da se smanji atribut ammo (koji označava broj preostalih grudvi), "
            "za 1, ako atribut ammo NIJE 0 pre klika") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        Player_show* ps = new Player_show(x, y);

        QKeyEvent* ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Space, Qt::NoModifier);

        const auto oldAmmo = ps->getPlayerInfo()->getAmmo();

        // Act
        ps->keyPressEvent(ev);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getAmmo() == oldAmmo - 1);


        delete ps;
        delete ev;

    }


    SECTION("Kada je pritisnut spacebar, NE bi trebalo da se smanji atribut ammo (koji označava broj preostalih grudvi), "
            "za 1, ako atribut ammo JESTE 0 pre klika") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        Player_show* ps = new Player_show(x, y);

        QKeyEvent* ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Space, Qt::NoModifier);

        for (int i = 0; i < PLAYER_MAX_AMMO; ++i)
            ps->getPlayerInfo()->throwASnowball();

        const auto numOfAmmo = ps->getPlayerInfo()->getAmmo();

        // Act
        ps->keyPressEvent(ev);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getAmmo() == numOfAmmo);


        delete ps;
        delete ev;

    }


    SECTION("Kada je pusteno dugme leva strelica, trebalo bi da se menja atribut isMoving unutar Playera na FALSE") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        Player_show* ps = new Player_show(x, y);

        QKeyEvent* ev = new QKeyEvent(QEvent::KeyRelease, Qt::Key_Left, Qt::NoModifier);

        // Act
        ps->keyReleaseEvent(ev);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getIsMoving() == false);


        delete ps;
        delete ev;
    }

    SECTION("Kada je pusteno dugme desna strelica, trebalo bi da se menja atribut isMoving unutar Playera na FALSE") {

        // Arrange
        const double x = 0.0;
        const double y = 0.0;
        Player_show* ps = new Player_show(x, y);

        QKeyEvent* ev = new QKeyEvent(QEvent::KeyRelease, Qt::Key_Right, Qt::NoModifier);

        // Act
        ps->keyReleaseEvent(ev);

        // Assert
        REQUIRE(ps->getPlayerInfo()->getIsMoving() == false);


        delete ps;
        delete ev;
    }



}
