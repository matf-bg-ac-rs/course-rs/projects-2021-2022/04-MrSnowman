QT       += core gui \
            multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    code/button.cpp \
    code/collectable.cpp \
    code/collectable_show.cpp \
    code/collidable.cpp \
    code/enemy.cpp \
    code/enemy_show.cpp \
    code/entity.cpp \
    code/game.cpp \
    code/health_bar.cpp \
    code/health_text.cpp \
    code/level.cpp \
    code/live_entity.cpp \
    code/main.cpp \
    code/menu.cpp \
    code/object.cpp \
    code/player.cpp \
    code/player_show.cpp \
    code/settings.cpp \
    code/show.cpp \
    code/snowball_text.cpp \
    code/snowballammo.cpp \
    code/snowballammo_show.cpp \
    code/tile.cpp \
    code/tile_show.cpp

HEADERS += \
    code/collidable.h \
    code/configuration.h \
    code/button.h \
    code/collectable.h \
    code/collectable_show.h \
    code/enemy.h \
    code/enemy_show.h \
    code/entity.h \
    code/game.h \
    code/health_bar.h \
    code/health_text.h \
    code/level.h \
    code/live_entity.h \
    code/menu.h \
    code/object.h \
    code/player.h \
    code/player_show.h \
    code/settings.h \
    code/show.h \
    code/snowball_text.h \
    code/snowballammo.h \
    code/snowballammo_show.h \
    code/tile.h \
    code/tile_show.h

FORMS += \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    images.qrc \
    sound.qrc \
    txt.qrc

DISTFILES += \
    resources/img/background.jpg \
    resources/img/background_old.jpg \
    resources/img/carrot.png \
    resources/img/credits.jpg \
    resources/img/credits2.jpg \
    resources/img/cursor.png \
    resources/img/dialog_1.png \
    resources/img/dialog_2.png \
    resources/img/dialog_3.png \
    resources/img/dialog_template.png \
    resources/img/icicle.png \
    resources/img/icon.png \
    resources/img/loadingscreen.jpg \
    resources/img/mainmenu.jpg \
    resources/img/mainmenu_button.jpg \
    resources/img/mainmenu_button.png \
    resources/img/penguin_standing.png \
    resources/img/penguin_standing2.png \
    resources/img/penguin_walk1.png \
    resources/img/penguin_walk2.png \
    resources/img/settings_button.jpg \
    resources/img/settings_button_check_off.png \
    resources/img/settings_button_check_on.png \
    resources/img/settings_button_off.png \
    resources/img/settings_button_on.png \
    resources/img/snow_tile.png \
    resources/img/snowball.png \
    resources/img/snowball_old.png \
    resources/img/snowman.png \
    resources/img/snowman_old.png \
    resources/img/sound_button_off.png \
    resources/img/sound_button_on.png \
    resources/img/tile.png \
    resources/img/wolf_standing.png \
    resources/img/wolf_walk1.png

