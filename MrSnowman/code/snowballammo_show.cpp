#include "snowballammo_show.h"
#include "game.h"

SnowballAmmo_show::SnowballAmmo_show(const double &_x, const double &_y, const Direction &_dir)
{
    snowballInfo = new SnowballAmmo(_x, _y, _dir);
    setPixmap(QPixmap(":/resources/img/snowball.png").scaled(SNOWBALL_SCALE, SNOWBALL_SCALE));
    setPos(_x, _y);
    setZValue(-1);
    Game::instance()->getScene()->addItem(this);
}

SnowballAmmo_show::~SnowballAmmo_show()
{
    delete snowballInfo;
}

SnowballAmmo* SnowballAmmo_show::getSnowballInfo() const
{
    return snowballInfo;
}

void SnowballAmmo_show::setImg()
{
}

void SnowballAmmo_show::animate()
{
    getSnowballInfo()->move();
    setX(getSnowballInfo()->getX());
}

void SnowballAmmo_show::setSnowballInfo(SnowballAmmo *_snowballInfo)
{
    snowballInfo = _snowballInfo;
}


