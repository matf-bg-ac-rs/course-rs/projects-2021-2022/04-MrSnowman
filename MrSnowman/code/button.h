#ifndef BUTTON_H
#define BUTTON_H
#include <QPushButton>
#include <QFont>
#include <QIcon>
#include <QSize>
#include <QDebug>
#include "configuration.h"

class Button : public QPushButton
{
public:
    Button(QString text);
    ~Button();
};

#endif // BUTTON_H
