#ifndef COLLECTABLE_SHOW_H
#define COLLECTABLE_SHOW_H

#include <QGraphicsPixmapItem>
#include "collidable.h"
#include "collectable.h"

class Collectable_show: public Show, public Collidable
{
private:
    Collectable *collectable_info;
public:
    Collectable_show(Collectable_type type, const double &x, const double &y);
    ~Collectable_show();
    int getType();
    void collidingWithPlayer(Player_show* player) override;
    void collidingWithSnowball(SnowballAmmo_show* snowball) override;
    void setImg() override;
    void animate() override;

};

#endif // COLLECTABLE_SHOW_H
