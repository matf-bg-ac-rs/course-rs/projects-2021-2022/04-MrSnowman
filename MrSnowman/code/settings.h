#ifndef SETTINGS_H
#define SETTINGS_H

#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QString>
#include <QMap>
#include "button.h"

class Settings : public QGraphicsScene
{

private:
    QGraphicsTextItem *soundTxt;
    QGraphicsTextItem *windowTxt;
    Button *soundOn;
    Button *soundOff;
    Button *fullscreen;
    Button *windowed;
    Button *backButton;

public:
    Settings(const int &window_width, const int &window_height);
    ~Settings();
    Button* getSoundOnButton() const;
    Button* getSoundOffButton() const;
    Button* getFullscreenButton() const;
    Button* getWindowedButton() const;
    Button* getBackButton() const;
};

#endif // SETTINGS_H
