#include "player_show.h"

Player_show::Player_show(unsigned _x, unsigned _y, QGraphicsItem *parent)
{
    Q_UNUSED(parent)
    this->setPos(_x, _y);
    playerInfo = new Player(_x, _y, PLAYER_MAX_HP);
}

Player_show::~Player_show()
{
    delete playerInfo;
    for (auto& el : allSnowballsThrown)
        delete el;
}

void Player_show::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Left){
        playerInfo->changeDirection(Direction::LEFT);
        playerInfo->startMoving();
    }
    else if (event->key() == Qt::Key_Right) {
        playerInfo->changeDirection(Direction::RIGHT);
        playerInfo->startMoving();
    }
    else if (event->key() == Qt::Key_Up) {
        playerInfo->jump();
    }
    else if (event->key() == Qt::Key_Space) {
        if (getPlayerInfo()->getAmmo() > 0) {
            //TILE_SCALE-(SNOWBALL_SCALE/2) so the snowball fires from the middle of the player
            allSnowballsThrown.append(new SnowballAmmo_show(getPlayerInfo()->getX()+TILE_SCALE-(SNOWBALL_SCALE/2),
                                                            getPlayerInfo()->getY() + TILE_SCALE -(SNOWBALL_SCALE/2),
                                                            getPlayerInfo()->getMovingDirection()));
            getPlayerInfo()->throwASnowball();
        }
        if (getPlayerInfo()->getAmmo() == 0) {
            qDebug() << "Out of snowballs";
        }
    }
}

void Player_show::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Left || event->key() == Qt::Key_Right)
        playerInfo->stopMoving();
}

Player *Player_show::getPlayerInfo() const
{
    return playerInfo;
}

QList<SnowballAmmo_show *> &Player_show::getAllSnowballs()
{
    return allSnowballsThrown;
}

void Player_show::setPlayer(Player *_player)
{
    playerInfo = _player;
}

void Player_show::setImg()
{
    if(getPlayerInfo()->getIsFalling()){
        setPixmap(QPixmap(":/resources/img/snowman_"+QString::number(round(static_cast<int>(round(getPlayerInfo()->getY()))%100/100.0))+".png").scaled(PLAYER_WIDTH, PLAYER_HEIGHT));
    } else {
        setPixmap(QPixmap(":/resources/img/snowman_1.png").scaled(PLAYER_WIDTH, PLAYER_HEIGHT));
    }
    if (this->getPlayerInfo()->getMovingDirection() == Direction::LEFT){
        setPixmap(pixmap().transformed(QTransform().scale(-1, 1)));
    }
}

void Player_show::animate()
{
    getPlayerInfo()->move();
    setPos(getPlayerInfo()->getX(), getPlayerInfo()->getY());
    setImg();
}



