#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QString>

inline const unsigned PLAYER_MAX_HP = 3;

inline const unsigned IMUNITY_DURATION = 200;

inline const unsigned SNOWBALL_SCALE  = 50;
inline const double SNOWBALL_SPEED = 8.0;

inline const unsigned WOLF_WIDTH = 100;
inline const unsigned WOLF_HEIGHT = 50;
inline const double WOLF_DISTANCE = 1000.0;

inline const unsigned PENGUIN_WIDTH = 50;
inline const unsigned PENGUIN_HEIGHT = 50;
inline const double PENGUIN_DISTANCE = 500.0;

inline const unsigned ENEMY_SPEED = 2;

inline const unsigned WINDOW_WIDTH = 1920;
inline const unsigned WINDOW_HEIGHT = 1080;

inline const unsigned BUTTON_HEIGHT = 100;
inline const unsigned BUTTON_WIDTH = 400;

inline const unsigned MAX_SPEED = 10;

// Condition for width : MAX_SPEED + BLOC_SIZE > PLAYER_WIDTH (for holes)
inline const unsigned PLAYER_WIDTH = 100;
inline const unsigned PLAYER_HEIGHT = 100;
inline const double PLAYER_SPEED = 6.0;
inline const double PLAYER_JUMP_SPEED = 10.0;
inline const double PLAYER_JUMP_DURATION = 1.0;
inline const double PLAYER_FALLING_SPEED = 0.015625;
inline const unsigned PLAYER_MAX_AMMO = 5;

inline const unsigned TILE_SCALE = 50;

inline const unsigned CURSOR_SIZE = 30;

inline const unsigned MAP_HEIGHT = 2000;
inline const unsigned MAP_WIDTH = 1100;

inline const unsigned CAMERA_LEFT = 450;
inline const unsigned CAMERA_RIGHT = 800;

inline const unsigned PROJ_HEIGHT = 40;
inline const unsigned PROJ_WIDTH = 40;

inline const unsigned FLAG_HEIGHT = 300;
inline const unsigned FLAG_WIDTH = 120;

inline const unsigned GRAVITY = 9;

inline const unsigned FPS = 60;

inline const QString TITLE = QString("Mr. Snowman");

inline const unsigned BTN_GAP = 150;



#endif // CONFIGURATION_H
