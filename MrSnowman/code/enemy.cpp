#include "enemy.h"


Enemy::Enemy(const Enemy_type &_type, const double &_x, const double &_y, const unsigned &_hp)
    :Live_entity(_x, _y, _hp), type(_type)
{
    distancePassed = 0.0f;
    switch (type) {
        case Wolf:
            this->setSpeed(ENEMY_SPEED);
            maxDistance = WOLF_DISTANCE;
            break;
        case Penguin:
            this->setSpeed(ENEMY_SPEED);
            maxDistance = PENGUIN_DISTANCE;
            break;
    }
}

Enemy::~Enemy()
{
}

Enemy_type Enemy::getEnemyType() const
{
    return type;
}

const double &Enemy::getDistancePassed() const
{
    return distancePassed;
}

const double &Enemy::getMaxDistance() const
{
    return maxDistance;
}

void Enemy::setEnemyType(const Enemy_type &_type)
{
    type = _type;
}

void Enemy::move()
{

    distancePassed += getSpeed();
    int dir = (getMovingDirection() == Direction::LEFT) ? -1 : 1;
    setX(getX() + dir * getSpeed());
    if (distancePassed >= maxDistance) {
        distancePassed = 0;
        if (getMovingDirection() == Direction::LEFT)
            setMovingDirection(Direction::RIGHT);
        else
            setMovingDirection(Direction::LEFT);
    }

}

void Enemy::setDistancePassed(const double &_distancePassed)
{
    distancePassed = _distancePassed;
}

void Enemy::setMaxDistance(const double &_maxDistance)
{
    maxDistance = _maxDistance;
}
