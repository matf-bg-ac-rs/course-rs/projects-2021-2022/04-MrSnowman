#ifndef MENU_H
#define MENU_H

#include <QGraphicsScene>
#include "configuration.h"
#include "button.h"
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QString>
#include <QMap>
#include <QStyleHints>

class Menu : public QGraphicsScene
{

private :
    QList<QString> buttonNames;
    QMap<QString, Button *> allButtons;

public:
    Menu(const int &window_width, const int &window_height);
    ~Menu();
    QMap<QString, Button *> getAllButtons() const;
    Button* getQuitButton() const;
    Button* getSettingsButton() const;
    Button* getStartButton() const;
    Button* getCreditsButton() const;
};

#endif // MENU_H
