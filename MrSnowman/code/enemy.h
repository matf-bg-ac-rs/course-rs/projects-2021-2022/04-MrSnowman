#ifndef ENEMY_H
#define ENEMY_H

#include "live_entity.h"

enum Enemy_type
{
Wolf = 0,
Penguin = 1,
};

class Enemy : public Live_entity
{
public:
    Enemy(const Enemy_type &_type,const double &_x, const double &_y, const unsigned &_hp = 1);
    ~Enemy();

    Enemy_type getEnemyType() const;
    const double& getDistancePassed() const;
    const double& getMaxDistance() const;
    void move() override;

private:
    Enemy_type type;
    void setEnemyType(const Enemy_type &_type);
    double distancePassed;
    double maxDistance;
    void setDistancePassed(const double& _distancePassed);
    void setMaxDistance(const double& _maxDistance);
};

#endif // ENEMY_H
