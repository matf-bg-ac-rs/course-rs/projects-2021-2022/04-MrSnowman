#ifndef ENTITY_H
#define ENTITY_H

#include "object.h"

enum class Direction
{
    LEFT = -1,
    RIGHT = 1,
    UP
};

class Entity : public Object
{
protected:

    void setSpeed(const double &_speed);
    void setIsMoving(bool _isMoving);
    void setMovingDirection(const Direction &dir);

private:

    double speed;
    Direction movingDirection;
    bool isMoving;

public:

    Entity(const double &_x, const double &_y, const double &_speed = 10.0, const Direction &dir = Direction::RIGHT);
    virtual ~Entity();
    virtual void move();
    const Direction& getMovingDirection() const;
    double getSpeed() const;
    bool getIsMoving() const;


};

#endif // ENTITY_H
