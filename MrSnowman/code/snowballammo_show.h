#ifndef SNOWBALLAMMO_SHOW_H
#define SNOWBALLAMMO_SHOW_H

#include <QGraphicsPixmapItem>

#include "show.h"
#include "snowballammo.h"

class SnowballAmmo_show : public Show
{
public:
    SnowballAmmo_show(const double &_x, const double &_y, const Direction &_dir);
    ~SnowballAmmo_show();
    SnowballAmmo* getSnowballInfo() const;

    void setImg() override;
    void animate() override;

private:
    SnowballAmmo* snowballInfo;
    void setSnowballInfo(SnowballAmmo* _snowballInfo);
};

#endif // SNOWBALLAMMO_SHOW_H
