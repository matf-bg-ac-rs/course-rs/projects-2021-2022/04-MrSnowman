#ifndef SNOWBALLAMMO_H
#define SNOWBALLAMMO_H

#include "entity.h"

class SnowballAmmo : public Entity
{
public:
    SnowballAmmo(const double &_x, const double &_y, const Direction &_dir);
    void move() override;
};

#endif // SNOWBALLAMMO_H
