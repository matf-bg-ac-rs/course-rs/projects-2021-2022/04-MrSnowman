#include "settings.h"

Settings::Settings(const int &window_width, const int &window_height) : QGraphicsScene()
{
    this->setSceneRect(0,0,window_width,window_height);
    QImage background = QImage(":/resources/img/credits.jpg");
    background = background.scaled(window_width, window_height);
    this->setBackgroundBrush(background);

    soundTxt = new QGraphicsTextItem();
    soundTxt->setPlainText("Sound :");
    QFont font = QFont("Edo",50);
    font.setBold(true);
    soundTxt->setFont(font);
    soundTxt->setPos(700,150);
    this->addItem(soundTxt);

    windowTxt = new QGraphicsTextItem();
    windowTxt->setPlainText("Window size :");
    windowTxt->setFont(font);
    windowTxt->setPos(700,450);
    this->addItem(windowTxt);


    soundOn = new Button("");
    soundOn->resize(BUTTON_HEIGHT, BUTTON_HEIGHT);
    soundOn->setStyleSheet("border-image : url(:/resources/img/sound_button_on.png); background: transparent;");
    soundOn->move(1100, 150);
    this->addWidget(soundOn);

    soundOff = new Button("");
    soundOff->resize(BUTTON_HEIGHT, BUTTON_HEIGHT);
    soundOff->setStyleSheet("border-image : url(:/resources/img/sound_button_off.png); background: transparent;");
    soundOff->move(1400, 150);
    this->addWidget(soundOff);

    fullscreen = new Button("");
    fullscreen->resize(BUTTON_HEIGHT, BUTTON_HEIGHT);
    fullscreen->setStyleSheet("border-image : url(:/resources/img/settings_button_check_on.png); background: transparent;");
    fullscreen->move(1100, 450);
    this->addWidget(fullscreen);

    windowed = new Button("");
    windowed->resize(BUTTON_HEIGHT, BUTTON_HEIGHT);
    windowed->setStyleSheet("border-image : url(:/resources/img/settings_button.png); background: transparent;");
    windowed->move(1400, 450);
    this->addWidget(windowed);

    backButton = new Button("Back");
    backButton->move(100, 930);
    this->addWidget(backButton);



}

Settings::~Settings() {

    delete soundOn;
    delete soundOff;
    delete fullscreen;
    delete windowed;
    delete backButton;
    delete soundTxt;
    delete windowTxt;
}

Button *Settings::getSoundOnButton() const
{
    return soundOn;
}

Button *Settings::getSoundOffButton() const
{
    return soundOff;
}

Button *Settings::getFullscreenButton() const
{
    return fullscreen;
}

Button *Settings::getWindowedButton() const
{
    return windowed;
}

Button *Settings::getBackButton() const
{
    return backButton;
}

