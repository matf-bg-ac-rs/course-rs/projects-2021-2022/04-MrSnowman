#ifndef TILE_SHOW_H
#define TILE_SHOW_H

#include "tile.h"
#include "collidable.h"
#include <QGraphicsPixmapItem>

class Tile_show : public QGraphicsPixmapItem, public Collidable
{
private:
    Tile *tileInfo;
public:
    Tile_show(const QString &img_source, const double &_x, const double &_y, const bool &_isCollidable = true, const bool &_isLethal = false);
    ~Tile_show();
    Tile* getTileInfo();
    void collidingWithPlayer(Player_show* player) override;
    void collidingWithSnowball(SnowballAmmo_show* snowball) override;
};

#endif // TILE_SHOW_H
