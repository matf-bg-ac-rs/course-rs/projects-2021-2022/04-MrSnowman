#ifndef OBJECT_H
#define OBJECT_H

#include <iostream>
//#include <QDebug>
#include "configuration.h"

class Object
{
protected:
    bool walkable;
    void setX(const double &_x);
    void setY(const double &_y);
    void setIsCollidable(const bool &_isCollidable);

private:
    double x;
    double y;
    bool isCollidable;

public:
    Object();
    Object(const double &_x, const double &_y, const bool &_isCollidable = true);
    virtual ~Object();
    double getX() const;
    double getY() const;
    const bool& getIsCollidable() const;

    bool isWalkable()   {return walkable;}
};

#endif // OBJECT_H
