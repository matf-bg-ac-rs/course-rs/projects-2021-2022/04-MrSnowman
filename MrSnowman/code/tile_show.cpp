#include "tile_show.h"
#include "game.h"

Tile_show::Tile_show(const QString &img_source, const double &_x, const double &_y, const bool &_isCollidable, const bool &_isLethal)
{
    QPixmap img = QPixmap(img_source);
    img = img.scaled(TILE_SCALE,TILE_SCALE);
    setPixmap(img);
    setPos(_x, _y);
    Game::instance()->getScene()->addItem(this);
    tileInfo = new Tile(_x, _y, _isCollidable, _isLethal);
}

Tile_show::~Tile_show()
{
    delete tileInfo;
}

Tile *Tile_show::getTileInfo()
{
    return tileInfo;
}

void Tile_show::collidingWithPlayer(Player_show *player)
{
    if(getTileInfo()->getIsLethal()){
        player->getPlayerInfo()->takeDamage(PLAYER_MAX_HP);
        return;
    }

    if (player->getPlayerInfo()->getY() + PLAYER_HEIGHT < getTileInfo()->getY() + 50){
        player->getPlayerInfo()->stopFalling();
        player->getPlayerInfo()->unprepareToFall();
        player->getPlayerInfo()->changePosition(player->getPlayerInfo()->getX(),getTileInfo()->getY()-PLAYER_HEIGHT+1);
    }
    else if (player->getPlayerInfo()->getY() - 10 > getTileInfo()->getY()){
        player->getPlayerInfo()->stopFalling();
        player->getPlayerInfo()->startFalling();
        player->getPlayerInfo()->changePosition(player->getPlayerInfo()->getX(),getTileInfo()->getY()+TILE_SCALE);
    }
    else if (player->getPlayerInfo()->getX() < getTileInfo()->getX() &&
        player->getPlayerInfo()->getMovingDirection() == Direction::RIGHT){

        player->getPlayerInfo()->stopMoving();
        player->getPlayerInfo()->changePosition(getTileInfo()->getX()-PLAYER_WIDTH,player->getPlayerInfo()->getY());
    }
    else if (player->getPlayerInfo()->getX() > getTileInfo()->getX() &&
        player->getPlayerInfo()->getMovingDirection() == Direction::LEFT){

        player->getPlayerInfo()->stopMoving();
        player->getPlayerInfo()->changePosition(getTileInfo()->getX()+TILE_SCALE,player->getPlayerInfo()->getY());
    }
}

void Tile_show::collidingWithSnowball(SnowballAmmo_show *snowball)
{
    snowball->makeToBeRemoved();
}


