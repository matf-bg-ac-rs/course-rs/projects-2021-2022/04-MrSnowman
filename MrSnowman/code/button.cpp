#include "button.h"

Button::Button(QString text)
{
    this->setText(text);
    this->resize(BUTTON_WIDTH,BUTTON_HEIGHT);
    QFont font("Magnolia Sky");
    font.setBold(true);
    font.setPixelSize(29);
    setFont(font);
    //set image as border image to fit nicely insted of backgorund image
    //background: transparent makes the button transparent so only the background image is visible
    this->setStyleSheet("border-image : url(:/resources/img/mainmenu_button.png); color: white; background: transparent;");

}
Button::~Button()
{
}
