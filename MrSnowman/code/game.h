#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QTimer>
#include <QSoundEffect>
#include <QMediaPlayer>
#include <QtMultimedia/QAudioOutput>
#include <QImage>
#include <QApplication>
#include <QDebug>
#include <QWidget>
#include "configuration.h"
#include "menu.h"
#include "settings.h"
#include "button.h"
#include "level.h"
#include "snowball_text.h"
#include "health_text.h"
#include "health_bar.h"
#include "collectable_show.h"

enum game_state{READY, RUNNING, DIALOGUE, PAUSE, GAME_OVER};

class Game : public QGraphicsView
{

    Q_OBJECT

private:
    Game();
    static Game* unique_instance;

    unsigned width;
    unsigned height;
    QGraphicsScene *scene;
    QImage background;
    QMediaPlayer * musicPlayer;
    QSoundEffect mainTrack;
    QAudioOutput *audioOutput;
    QTimer *gameEngine;

    Menu *menu;
    Settings *settings;
    Level *level;
    QGraphicsPixmapItem *dialogScreen;
    Snowball_text *snowballText;
    Health_text *healthText;
    Health_bar *healthBar;
    Player_show *player;

    game_state currentState;
    QList<Button*> pauseButtons;
    Button* pauseBackground;
    unsigned levelID;
    void setWidth(unsigned &w);
    void setHeight(unsigned &h);
    void initializePauseButtons();

public:
    static Game* instance();
    QGraphicsScene* getScene();
    Player_show* getPlayer();
    void playMusic(QString qrcPath);
    void resizeEvent(QResizeEvent *event) override;
    const unsigned& getWidth() const;
    const unsigned& getHeight() const;
    Level* getLevel() const;
    Menu* getMenu() const;
    game_state getCurrentState() const;
    void setCurrentState(game_state newState);
    unsigned getLevelID();
    bool musicPaused;
    ~Game();

public slots:

    void open_menu();
    void quit_app();
    void start_game();
    void end_game();
    void open_settings();
    void open_credits();
    void open_old_scene();

    void move_camera();
    void move_player();
    void move_enemies();
    void move_snowball();
    void position_text();

    void check_player_collision();
    void check_snowball_collision();

    void play_music();
    void pause_music();
    void show_fullscreen();
    void show_windowed();
    void keyPressEvent(QKeyEvent * event) override;
    void pause_game();

    void remove_items();

};



#endif // GAME_H
