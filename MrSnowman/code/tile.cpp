#include "tile.h"

Tile::Tile(const double &_x, const double &_y, const bool &_isCollidable, const bool &_isLethal)
    :Object(_x, _y, _isCollidable), isLethal(_isLethal)
{
    walkable = true;
}

const bool &Tile::getIsLethal() const
{
    return isLethal;
}

void Tile::setIsLethal(const bool &_isLethal)
{
    isLethal = _isLethal;
}

Tile::~Tile()
{
}
