#include "enemy_show.h"
#include "game.h"

Enemy_show::Enemy_show(Enemy_type type, const double &x, const double &y)
    : Show()
{
    this->enemyInfo = new Enemy(type, x, y);
    setPos(x,y);
    Game::instance()->getScene()->addItem(this);
}

Enemy_show::~Enemy_show()
{
    delete enemyInfo;
}

Enemy *Enemy_show::getEnemyInfo() const
{
    return enemyInfo;
}

void Enemy_show::setImg()
{  
    switch (this->getEnemyInfo()->getEnemyType()) {
        case Wolf:
            setPixmap(QPixmap(":/resources/img/wolf_walk.png").scaled(WOLF_WIDTH, WOLF_HEIGHT));
            break;
        case Penguin:
            setPixmap(QPixmap(":/resources/img/penguin_walk.png").scaled(PENGUIN_WIDTH, PENGUIN_HEIGHT));
            break;
    }
    if (this->getEnemyInfo()->getMovingDirection() == Direction::LEFT){
        setPixmap(pixmap().transformed(QTransform().scale(-1, 1)));
    }

}

void Enemy_show::animate()
{
    setImg();
    getEnemyInfo()->move();
    setX(getEnemyInfo()->getX());
    switch (this->getEnemyInfo()->getEnemyType()) {
        case Wolf:
            setPixmap(QPixmap(":/resources/img/wolf_walk_"+QString::number(round(static_cast<int>(round(getEnemyInfo()->getX()))%50/50.0))+".png").scaled(WOLF_WIDTH, WOLF_HEIGHT));
            break;
        case Penguin:
            setPixmap(QPixmap(":/resources/img/penguin_walk_"+QString::number(round(static_cast<int>(round(getEnemyInfo()->getX()))%50/50.0))+".png").scaled(PENGUIN_WIDTH, PENGUIN_HEIGHT));
            break;
    }
    if (this->getEnemyInfo()->getMovingDirection() == Direction::LEFT){
        setPixmap(pixmap().transformed(QTransform().scale(-1, 1)));
    }
}

void Enemy_show::collidingWithPlayer(Player_show *player)
{
    if (player->getPlayerInfo()->getImunityDuration() == 0) {
        player->getPlayerInfo()->takeDamage(1);
        player->getPlayerInfo()->makeImune();
    }
}

void Enemy_show::collidingWithSnowball(SnowballAmmo_show *snowball)
{
    snowball->makeToBeRemoved();
    makeToBeRemoved();
}




