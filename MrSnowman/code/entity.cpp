#include "entity.h"

Entity::Entity(const double &_x, const double &_y, const double &_speed, const Direction &dir)
    :Object(_x, _y), speed(_speed), movingDirection(dir)
{
    isMoving = false;
}

Entity::~Entity()
{
}

void Entity::move()
{
    if (isMoving) {
        if (movingDirection == Direction::RIGHT)
            setX(getX() + speed);
        else if (movingDirection == Direction::LEFT)
            setX(getX() - speed);
    }
}


const Direction& Entity::getMovingDirection() const
{
    return movingDirection;
}

double Entity::getSpeed() const
{
    return speed;
}

bool Entity::getIsMoving() const
{
    return isMoving;
}

void Entity::setSpeed(const double &_speed)
{
    speed = _speed;
}

void Entity::setIsMoving(bool _isMoving)
{
    isMoving = _isMoving;
}

void Entity::setMovingDirection(const Direction &dir)
{
    movingDirection = dir;
}


