#include "show.h"

Show::Show(QGraphicsPixmapItem* parent)
    :QGraphicsPixmapItem(parent)
{
    toBeRemoved = false;
}
bool Show::getToBeRemoved(){
    return toBeRemoved;
}
void Show::setToBeRemoved(const bool &_toBeRemoved){
    toBeRemoved = _toBeRemoved;
}
void Show::makeToBeRemoved(){
    setToBeRemoved(true);
}
