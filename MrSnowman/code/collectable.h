#ifndef COLLECTABLE_H
#define COLLECTABLE_H

#include "object.h"
#include <QDebug>

enum Collectable_type
{
SNOWBALL = 0,
CARROT = 1,
};

class Collectable : public Object
{
public:
    Collectable(Collectable_type type);
    ~Collectable();
    Collectable_type type;
};

#endif // COLLECTABLE_H
