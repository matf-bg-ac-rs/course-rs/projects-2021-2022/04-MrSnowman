#include "snowballammo.h"

SnowballAmmo::SnowballAmmo(const double &_x, const double &_y, const Direction &_dir)
    :Entity(_x, _y, SNOWBALL_SPEED, _dir)
{
}

void SnowballAmmo::move()
{
    int dir = static_cast<int>(getMovingDirection());
    setX(getX() + dir * getSpeed());
}


