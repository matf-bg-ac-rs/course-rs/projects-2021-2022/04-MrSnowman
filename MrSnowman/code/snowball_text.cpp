#include "game.h"
#include "snowball_text.h"

Snowball_text::Snowball_text()
{
    setPlainText(QString("Snowball:") + QString::number(5));
    setDefaultTextColor(Qt::black);
    setFont(QFont("Edo",40));
}

void Snowball_text::setSnowballNumber(unsigned number){
    snowballNumber = number;
    setPlainText(QString("Snowball: ") + QString::number(snowballNumber));
}

unsigned Snowball_text::getSnowballNumber(){
    return snowballNumber;
}

Snowball_text::~Snowball_text()
{
}

