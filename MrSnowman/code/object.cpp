#include "object.h"
#include "game.h"

Object::Object()
{
    walkable = false;
}

Object::Object(const double &_x, const double &_y,  const bool &_isCollidable)
    :x(_x), y(_y), isCollidable(_isCollidable)
{
     walkable = false;
}

Object::~Object()
{
}

double Object::getX() const
{
    return x;
}

double Object::getY() const
{
    return y;
}

const bool &Object::getIsCollidable() const
{
    return isCollidable;
}

void Object::setX(const double &_x)
{
    x = _x;
}

void Object::setY(const double &_y)
{
    y = _y;
}

void Object::setIsCollidable(const bool &_isCollidable)
{
    isCollidable = _isCollidable;
}




