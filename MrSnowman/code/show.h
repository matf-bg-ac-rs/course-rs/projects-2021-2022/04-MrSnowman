#ifndef SHOW_H
#define SHOW_H

#include <QGraphicsPixmapItem>

class Show : public QGraphicsPixmapItem
{
public:
    Show(QGraphicsPixmapItem* parent = nullptr);
    virtual void setImg() = 0;
    virtual void animate() = 0;
    bool getToBeRemoved();
    void makeToBeRemoved();
private:
    bool toBeRemoved;
    void setToBeRemoved(const bool &_toBeRemoved);
};
#endif // SHOW_H
