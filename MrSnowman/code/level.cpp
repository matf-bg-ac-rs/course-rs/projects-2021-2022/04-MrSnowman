#include "level.h"

Level::Level()
= default;

Level::~Level()
{
    tiles.clear();
    enemies.clear();
    collectables.clear();
}

Player_show *Level::loadLevel(unsigned levelID, QGraphicsScene *scene)
{
    scene->setSceneRect(0,0,1920*8,1080*2);
    QString file = ":/resources/txt/lvl" + QString::number(levelID, 10) + ".txt";
    scene->setBackgroundBrush((QBrush(QImage(":/resources/img/background.jpg").scaled(1920,1080))));

    return parseLevel(file);
}

QList<Enemy_show *> &Level::getEnemies()
{
    return enemies;
}

QList<Collectable_show *> &Level::getCollectables()
{
    return collectables;
}

const QList<Tile_show *> &Level::getTiles() const
{
    return tiles;
}

Player_show* Level::parseLevel(const QString& filename)
{
    QFile file(filename);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "File not open";
}

    QTextStream in(&file);

    int tile_x_coord = 0;
    int tile_y_coord = 0;

    float tile_dim = TILE_SCALE;

    Player_show* player = nullptr;
    enemies.clear();
    tiles.clear();
    collectables.clear();

    while(!in.atEnd()){
        QString line = in.readLine();
        for(auto c : line){
            if(c == 'F')
            {
                tiles.append(new Tile_show(":/resources/img/snow_tile.png",tile_x_coord, tile_y_coord));
            }
            else if(c == 'B')
            {
                tiles.append(new Tile_show(":/resources/img/tile.png",tile_x_coord, tile_y_coord));
            }
            else if(c == 'I')
            {
                tiles.append(new Tile_show(":/resources/img/icicle.png",tile_x_coord,tile_y_coord,true,true));
            }
            else if(c == 'C')
            {
                collectables.append(new Collectable_show(CARROT, tile_x_coord, tile_y_coord));
            }
            else if(c == 'S')
            {
                collectables.append(new Collectable_show(SNOWBALL, tile_x_coord, tile_y_coord));
            }
            else if(c == 'P')
            {
                player = new Player_show(tile_x_coord, tile_y_coord-tile_dim);
                player->setFlag(QGraphicsItem::ItemIsFocusable);
                player->setFocus();
            }
            else if(c == 'W')
            {
                enemies.append(new Enemy_show(Wolf,tile_x_coord,tile_y_coord));
            }
            else if(c == 'E')
            {
                enemies.append(new Enemy_show(Penguin,tile_x_coord,tile_y_coord));
            }
            else if(c == 'Z')
            {
                nextLevel = tile_x_coord;
            }


            tile_x_coord += tile_dim;
        }

        tile_y_coord += tile_dim;
        tile_x_coord = 0;
    }

    file.close();
    return player;
}









