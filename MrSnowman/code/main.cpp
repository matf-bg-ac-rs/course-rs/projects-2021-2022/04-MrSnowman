#include <QApplication>
#include <QGraphicsScene>
#include <QTimer>
#include <QDebug>
#include <QGraphicsView>
#include <QSplashScreen>
#include "configuration.h"
#include "game.h"
#include <QScreen>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QSplashScreen *splash = new QSplashScreen;
    splash->setPixmap(QPixmap(":/resources/img/loadingscreen.jpg"));
    splash->show();

    Game::instance();

    QTimer::singleShot(2500, splash, SLOT(close()));

    return app.exec();
}
