#ifndef HEALTH_BAR_H
#define HEALTH_BAR_H

#include "configuration.h"
#include <QGraphicsPixmapItem>
#include <QDebug>

class Health_bar: public QGraphicsPixmapItem
{
public:
    Health_bar();
    void setImage(QPixmap hp);
    void setHealth(const unsigned &hp);
    ~Health_bar();

};

#endif // HEALTH_BAR_H
