#include "player.h"


Player::Player(const double &_x, const double &_y, const unsigned &_hp)
    :Live_entity(_x, _y, _hp)
{
    ammo = PLAYER_MAX_AMMO;
    jumpDuration = 0.0f;
    verticalSpeed = 0.0f;
    isFalling = false;
    setSpeed(PLAYER_SPEED);
    fallingSpeed = PLAYER_FALLING_SPEED;
    imunityDuration = 0;
    baseImunityDuration = IMUNITY_DURATION;
    shouldFall = true;
}

Player::~Player()
{
}

unsigned Player::getAmmo() const
{
    return ammo;
}

bool Player::getIsFalling() const
{
    return isFalling;
}

double Player::getJumpDuration() const
{
    return jumpDuration;
}

double Player::getVerticalSpeed() const
{
    return verticalSpeed;
}

unsigned Player::getImunityDuration() const
{
    return imunityDuration;
}

double Player::getFallingSpeed() const
{
    return fallingSpeed;
}

bool Player::getShouldFall() const
{
    return shouldFall;
}

void Player::prepareToFall()
{
    setShouldFall(true);
}

void Player::unprepareToFall()
{
    setShouldFall(false);
}

void Player::setAmmo(const unsigned &_ammo)
{
    ammo = _ammo;
}

void Player::setFallingSpeed(const double &_fallingSpeed)
{
    fallingSpeed = _fallingSpeed;
}

void Player::setIsFalling(const bool &_isFalling)
{
    isFalling = _isFalling;
}

void Player::setJumpDuration(const double &_jumpDuration)
{
    jumpDuration = _jumpDuration;
}

void Player::setShouldFall(const double &_shouldFall)
{
    shouldFall = _shouldFall;
}

void Player::setVerticalSpeed(const double &_verticalSpeed)
{
    verticalSpeed = _verticalSpeed;
}

void Player::setImunityDuration(const unsigned &_imunityDuration)
{
    imunityDuration = _imunityDuration;
}

void Player::jump()
{
    if(!isFalling){
        startFalling();
        setJumpDuration(PLAYER_JUMP_DURATION);
    }
}

void Player::calculateVerticalSpeed()
{
    setVerticalSpeed(PLAYER_JUMP_SPEED*getJumpDuration());
    setJumpDuration(getJumpDuration() - fallingSpeed);
}

void Player::startFalling()
{
    setIsFalling(true);
}

void Player::stopFalling()
{
    setVerticalSpeed(0.0f);
    setJumpDuration(0.0f);
    setIsFalling(false);
}

void Player::startMoving()
{
    setIsMoving(true);
}

void Player::stopMoving()
{
    setIsMoving(false);
}

void Player::changeDirection(const Direction &dir)
{
    setMovingDirection(dir);
}

void Player::throwASnowball()
{
    if (getAmmo() != 0){
        setAmmo(getAmmo() - 1);
    }
}

void Player::move()
{
    if(getIsMoving()){
        if(getX()+static_cast<int>(getMovingDirection())*getSpeed() > 0){
            setX(getX()+static_cast<int>(getMovingDirection())*getSpeed());
        } else {
            setX(0);
        }
    }
    if(getIsFalling()){
        calculateVerticalSpeed();
    }
    setY(getY()-getVerticalSpeed());
    if(getShouldFall()){
        startFalling();
    }
    prepareToFall();
}

void Player::changePosition(const double &x, const double &y)
{
    setX(x);
    setY(y);
    return;
}

void Player::changeIsMoving(const bool &_isMoving)
{
    setIsMoving(_isMoving);
}

void Player::increaseHealth()
{
    if(getHP() < PLAYER_MAX_HP){
        setHp(getHP() + 1);
    }
}

void Player::increaseAmmo()
{
    if(getAmmo() < PLAYER_MAX_AMMO){
        setAmmo(getAmmo() + 1);
    }
}

void Player::makeImune()
{
    setImunityDuration(baseImunityDuration);
}

void Player::decreaseImunity()
{
    if (getImunityDuration() > 0){
        setImunityDuration(getImunityDuration()-1);
    }
}



