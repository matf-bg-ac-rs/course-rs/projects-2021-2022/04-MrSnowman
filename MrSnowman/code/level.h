#ifndef LEVEL_H
#define LEVEL_H

#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QFile>
#include <QDebug>
#include <QTimer>
#include <QList>
#include "player_show.h"
#include "tile_show.h"
#include "enemy_show.h"
#include "collectable_show.h"
#include "configuration.h"

class Level : public QGraphicsScene, public QString
{
public:
    Level();
    ~Level();
    Player_show* loadLevel(unsigned levelID, QGraphicsScene *scene);
    QList<Enemy_show *>& getEnemies();
    QList<Collectable_show *>& getCollectables();
    const QList<Tile_show *>& getTiles() const;
    unsigned nextLevel;

private:
    Player_show* parseLevel(const QString& filename);
    QList<Tile_show *> tiles;
    QList<Enemy_show *> enemies;
    QList<Collectable_show *> collectables;
};

#endif // LEVEL_H
