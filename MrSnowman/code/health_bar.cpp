#include "health_bar.h"
#include "game.h"

Health_bar::Health_bar()
{
    setHealth(3);
}

void Health_bar::setHealth(const unsigned &hp)
{
    setPixmap(QPixmap(":/resources/img/hp_" +QString::number(hp, 10) + ".png").scaled(100,40));
}
Health_bar::~Health_bar()
{
}
