#include "collectable_show.h"
#include "game.h"

Collectable_show::Collectable_show(Collectable_type type, const double &x, const double &y)
{
    QPixmap img;
    this->collectable_info = new Collectable(type);
    switch (type) {
        case SNOWBALL:
            img = QPixmap(":/resources/img/snowball.png");
            break;
        case CARROT:
            img = QPixmap(":/resources/img/carrot.png");
            break;
    }
    img = img.scaled(TILE_SCALE,TILE_SCALE);
    setPixmap(img);
    setPos(x, y);
    Game::instance()->getScene()->addItem(this);
}

Collectable_show::~Collectable_show()
{
    delete collectable_info;
}

int Collectable_show::getType()
{
    return this->collectable_info->type;
}

void Collectable_show::collidingWithPlayer(Player_show *player)
{
    if (getType() == CARROT) {
        if (player->getPlayerInfo()->getHP() != PLAYER_MAX_HP)
        {
            player->getPlayerInfo()->increaseHealth();
            makeToBeRemoved();
        }
    }
    else if(getType() == SNOWBALL){
        if (player->getPlayerInfo()->getAmmo() != PLAYER_MAX_AMMO){
            player->getPlayerInfo()->increaseAmmo();
            makeToBeRemoved();
        }
    }
}

void Collectable_show::collidingWithSnowball(SnowballAmmo_show *snowball)
{
    Q_UNUSED(snowball)
}

void Collectable_show::setImg()
{
}

void Collectable_show::animate()
{
}

