#include "live_entity.h"

Live_entity::Live_entity(const double &_x, const double &_y, const unsigned &_hp, const double &_speed, const bool &_isAlive)
    :Entity(_x, _y, _speed), hp(_hp), isAlive(_isAlive)
{
}

Live_entity::~Live_entity()
{
}

void Live_entity::takeDamage(const unsigned &dmg)
{
    if (hp <= dmg){
        hp = 0;
        isAlive = false;
    }
    else
        hp -= dmg;
}

bool Live_entity::checkDeath() const
{
    return !isAlive;
}

unsigned Live_entity::getHP() const
{
    return hp;
}

void Live_entity::setHp(const unsigned &_hp)
{
    hp = _hp;
}


