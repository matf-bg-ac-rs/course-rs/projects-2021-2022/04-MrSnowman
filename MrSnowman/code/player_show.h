#ifndef PLAYER_SHOW_H
#define PLAYER_SHOW_H

#include <QGraphicsItem>
#include <QPixmap>
#include <QKeyEvent>
#include <QObject>
#include <Qt>
#include <QString>
#include <QList>
#include "snowballammo_show.h"
#include "player.h"
#include "show.h"

class Player_show : public QObject, public Show
{
    Q_OBJECT

private:
    Player* playerInfo;
    QList<SnowballAmmo_show *> allSnowballsThrown;
    void setPlayer(Player* _player);

public:

    void setImg() override;
    void animate() override;

    Player_show(unsigned _x, unsigned _y, QGraphicsItem *parent = nullptr);
    ~Player_show();
    Player* getPlayerInfo() const;
    QList<SnowballAmmo_show *>& getAllSnowballs();

public slots:
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

};

#endif // PLAYER_SHOW_H
