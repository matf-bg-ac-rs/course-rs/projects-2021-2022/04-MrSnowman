#include "menu.h"

Menu::Menu(const int &window_width, const int &window_height)
{
   int xButton = 900; //2.133 RATION
   int yStartButton = 300; // 3.6 RATIO
   this->setSceneRect(0,0,window_width,window_height);
   QImage background = QImage(":/resources/img/mainmenu.jpg");
   background = background.scaled(window_width,window_height);
   this->setBackgroundBrush(background);

   buttonNames = {"Start", "Settings", "Credits", "Exit"};

   for (QString &name : buttonNames) {
       auto *new_button = new Button(name);
       new_button->move(xButton, yStartButton + (new_button->height()));
       yStartButton += BTN_GAP;
       this->addWidget(new_button);
       allButtons[name] = new_button;
   }
}

Menu::~Menu()
{
   for(auto *element : qAsConst(allButtons)) {
       delete element;
   }
   allButtons.clear();
}

QMap<QString, Button *> Menu::getAllButtons() const
{
    return allButtons;
}

Button *Menu::getQuitButton() const
{
    return allButtons["Exit"];
}

Button *Menu::getSettingsButton() const
{
    return allButtons["Settings"];
}

Button *Menu::getStartButton() const
{
    return allButtons["Start"];
}

Button *Menu::getCreditsButton() const
{
    return allButtons["Credits"];
}


