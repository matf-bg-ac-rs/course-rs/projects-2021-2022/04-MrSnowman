#include "game.h"
#include <cstdlib>
#include <QGraphicsItem>

Game* Game:: unique_instance = 0;
Game* Game::instance()
{
    if(unique_instance == 0)
        unique_instance = new Game();
    return unique_instance;
}


Game::Game()
{

    //setting title and game icon
    setWindowTitle("Mr. Snowman");
    setWindowIcon(QIcon(":/resources/img/icon.png"));
    scene = new QGraphicsScene();
    setScene(scene);
    QApplication::setOverrideCursor(Qt::BlankCursor);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();
    //initialize menu and settings scene
    menu = new Menu(screenGeometry.width(),screenGeometry.height());
    settings = new Settings(screenGeometry.width(),screenGeometry.height());

    dialogScreen = new QGraphicsPixmapItem();

    //initialize music player
    musicPlayer = new QMediaPlayer();
    audioOutput = new QAudioOutput();

    //initialize timer
    gameEngine = new QTimer();
    gameEngine->setInterval(16);
    player = new Player_show(0,0);
    connect(gameEngine, SIGNAL(timeout()), this, SLOT(move_camera()));
    connect(gameEngine, SIGNAL(timeout()), this, SLOT(move_player()));
    connect(gameEngine, SIGNAL(timeout()), this, SLOT(move_enemies()));
    connect(gameEngine, SIGNAL(timeout()), this, SLOT(move_snowball()));
    connect(gameEngine, SIGNAL(timeout()), this, SLOT(check_player_collision()));
    connect(gameEngine, SIGNAL(timeout()), this, SLOT(check_snowball_collision()));
    connect(gameEngine,SIGNAL(timeout()), this, SLOT(position_text()));
    connect(gameEngine,SIGNAL(timeout()), this, SLOT(remove_items()));


    this->setTransformationAnchor(QGraphicsView::NoAnchor);
    musicPaused = false;
    open_menu();
    connect(menu->getQuitButton(), SIGNAL(clicked()), this, SLOT(quit_app()));
    connect(menu->getStartButton(), SIGNAL(clicked()), this, SLOT(start_game()));
    connect(menu->getSettingsButton(), SIGNAL(clicked()), this, SLOT(open_settings()));
    connect(menu->getCreditsButton(), SIGNAL(clicked()), this, SLOT(open_credits()));
    QTimer::singleShot(2500,this,SLOT(showFullScreen()));
    level = new Level();
    currentState = READY;

}

void Game::start_game()
{
    gameEngine->start();
    scene->clear();
    QApplication::setOverrideCursor(Qt::BlankCursor);
    player = this->getLevel()->loadLevel(levelID, scene);
    setScene(scene);
    if (!musicPaused)
        playMusic("qrc:/resources/sound/level_soundtrack.mp3");

    if(!player)
    {
       scene->setBackgroundBrush(QBrush(Qt::red));
       QGraphicsTextItem* text = scene->addText("Error when loading level");
       text->setPos(300,90);
       centerOn(text);
    }

    scene->addItem(player);

    snowballText = new Snowball_text();
    healthText = new Health_text();
    healthBar = new Health_bar();

    scene->addItem(snowballText);
    scene->addItem(healthText);
    scene->addItem(healthBar);

    centerOn(player);

    //setting dialog screen
    dialogScreen->setPixmap(QPixmap(":/resources/img/dialog_" + QString::number(levelID, 10) + ".png").scaled(viewport()->width()+6,viewport()->height()+6));
    dialogScreen->setZValue(2);
    dialogScreen->setPos(QGraphicsView::mapToScene(0,0).x(),QGraphicsView::mapToScene(0,0).y());
    gameEngine->stop();
    pause_music();
    scene->addItem(dialogScreen);

    currentState = DIALOGUE;
    initializePauseButtons();

}

void Game::end_game()
{
    QPointF view_start = QGraphicsView::mapToScene(0,0);
    currentState = GAME_OVER;
    gameEngine->stop();
    pause_music();
    if (this->getPlayer()->getPlayerInfo()->checkDeath()){
        delete player;
        scene->clear();
        dialogScreen->setPixmap(QPixmap(":/resources/img/dead_scene.jpg").scaled(viewport()->width()+6,viewport()->height()+6));
    } else {
        delete player;
        scene->clear();
        dialogScreen->setPixmap(QPixmap(":/resources/img/end_scene.png").scaled(viewport()->width()+6,viewport()->height()+6));
    }
    dialogScreen->setPos(view_start.x(), view_start.y());
    dialogScreen->setZValue(2);
    scene->addItem(dialogScreen);
}

void Game::quit_app()
{
    setAttribute(Qt::WA_DeleteOnClose);
    this->close();
}

void Game::move_camera()
{
    centerOn(player);
}

void Game::move_player()
{
    if (currentState == GAME_OVER){
        return;
    }
    player->animate();
    //  Check if we reached the end of level or won
    if (player->getPlayerInfo()->getX() > getLevel()->nextLevel) {
        if (levelID == 3){
            end_game();
        }else {
            levelID++;
            start_game();
        }
        return;
    }
    if(this->getPlayer()->getPlayerInfo()->getImunityDuration() == 0){
        this->getPlayer()->setOpacity(1.0);
    } else {
        this->getPlayer()->setOpacity(round((this->getPlayer()->getPlayerInfo()->getImunityDuration()%4)/4.0));
    }
    this->getPlayer()->getPlayerInfo()->decreaseImunity();

}

void Game::move_enemies()
{
    if (currentState == GAME_OVER){
        return;
    }
    for (auto &el : this->getLevel()->getEnemies()) {
        el->animate();
    }
}

void Game::move_snowball()
{
    if (currentState == GAME_OVER){
        return;
    }
    for (auto& el : getPlayer()->getAllSnowballs()) {
        el->animate();
        if ((el->getSnowballInfo()->getX() > this->getPlayer()->getPlayerInfo()->getX() + this->viewport()->width() / 2) ||
                (el->getSnowballInfo()->getX() < this->getPlayer()->getPlayerInfo()->getX() - this->viewport()->width() / 2)) {

            getPlayer()->getAllSnowballs().removeOne(el);
            delete el;
        }
    }
}

void Game::position_text(){
    if (currentState == GAME_OVER){
        return;
    }
    QPointF view_start = QGraphicsView::mapToScene(0,0);
    healthBar->setHealth(player->getPlayerInfo()->getHP());
    snowballText->setSnowballNumber(player->getPlayerInfo()->getAmmo());
    healthText->setPos(view_start.x(), view_start.y());
    healthBar->setPos(view_start.x()+100, view_start.y()+10);
    snowballText->setPos(view_start.x(), view_start.y()+45);

}

void Game::check_player_collision()
{
    if (currentState == GAME_OVER){
        return;
    }
    QList<QGraphicsItem *> collidingItems = this->getPlayer()->collidingItems();
    for (auto &el : collidingItems) {
        Collidable *c = dynamic_cast<Collidable*>(el);
        if(c){
            c->collidingWithPlayer(player);
        }
    }
    if (this->getPlayer()->getPlayerInfo()->checkDeath()) {
        end_game();
        return;
    }
}

void Game::check_snowball_collision()
{
    if (currentState == GAME_OVER){
        return;
    }
    for (auto &snowball : this->getPlayer()->getAllSnowballs()) {
        QList<QGraphicsItem *> collidingItems = snowball->collidingItems();

        for (auto &el : collidingItems) {
            Collidable *c = dynamic_cast<Collidable*>(el);
            if(c){
                c->collidingWithSnowball(snowball);
            }
        }
    }
}


void Game::open_menu()
{
    if (currentState == GAME_OVER || currentState == PAUSE)
        musicPaused = false;
    currentState = READY;
    // Show mouse cursor
    QPixmap curs = QPixmap(":/resources/img/cursor.png");
    curs.scaled(CURSOR_SIZE, CURSOR_SIZE);
    QCursor cursor(curs);
    QApplication::setOverrideCursor(cursor);
    if (!musicPaused)
        playMusic("qrc:/resources/sound/mainmenu.mp3");
    setScene(menu);
    // setting level id to 1 when the player loses game or finishes it, the game can start over from 1st lvl
    levelID = 1;

}

void Game::open_credits()
{
    scene->clear();
    scene->setSceneRect(0,0,viewport()->width(),viewport()->height());
    QImage background = QImage(":/resources/img/credits2.jpg");
    background = background.scaled(viewport()->width(),viewport()->height());
    scene->setBackgroundBrush(background);

    Button *back_button = new Button("Back");
    connect(back_button, SIGNAL(clicked()), this, SLOT(open_menu()));
    back_button->move(100, 930);
    scene->addWidget(back_button);
    setScene(scene);

}

void Game::open_settings()
{

    connect(settings->getSoundOnButton(), SIGNAL(clicked()), this, SLOT(play_music()));
    connect(settings->getSoundOffButton(), SIGNAL(clicked()), this, SLOT(pause_music()));
    connect(settings->getFullscreenButton(), SIGNAL(clicked()), this, SLOT(show_fullscreen()));
    connect(settings->getWindowedButton(), SIGNAL(clicked()), this, SLOT(show_windowed()));
    connect(settings->getBackButton(), SIGNAL(clicked()), this, SLOT(open_old_scene()));

    setScene(settings);
}

void Game::open_old_scene()
{
    if(currentState == READY)
        open_menu();
    else if(currentState == PAUSE){
        setScene(scene);
        centerOn(player);
    }
}

void Game::play_music()
{
    musicPlayer->play();
    musicPaused = false;
}

void Game::pause_music()
{
    musicPlayer->pause();
    musicPaused = true;
}

void Game::show_fullscreen()
{
    this->showFullScreen();
    settings->getFullscreenButton()->setStyleSheet("border-image : url(:/resources/img/settings_button_check_on.png); background: transparent;");
    settings->getWindowedButton()->setStyleSheet("border-image : url(:/resources/img/settings_button.png); background: transparent;");
}

void Game::show_windowed()
{
    this->showMaximized();
    settings->getFullscreenButton()->setStyleSheet("border-image : url(:/resources/img/settings_button.png); background: transparent;");
    settings->getWindowedButton()->setStyleSheet("border-image : url(:/resources/img/settings_button_check_on.png); background: transparent;");
}

void Game::keyPressEvent(QKeyEvent *event)
{
    if (currentState == GAME_OVER){
        if (event->key() == Qt::Key_E)
        {
            scene->removeItem(dialogScreen);
            open_menu();
        }
        return;
    }
    if ((currentState == RUNNING || currentState == PAUSE) && event->key() == Qt::Key_Escape)
    {
        pause_game();
    }
    else if (event->key() == Qt::Key_E && currentState == DIALOGUE)
    {
        scene->removeItem(dialogScreen);
        gameEngine->start();
        play_music();
        currentState = RUNNING;
    }
    else if (currentState == RUNNING)
    {
        this->player->keyPressEvent(event);
    }
}

void Game::initializePauseButtons()
{
    pauseButtons.clear();
    pauseBackground = new Button("");
    pauseBackground->resize(2*BUTTON_WIDTH,4*BTN_GAP);
    pauseBackground->setStyleSheet("border-image : url(:/resources/img/pause_background.png); background: transparent;");
    pauseBackground->hide();
    scene->addWidget(pauseBackground);

    Button *continue_btn = new Button("Continue");
    continue_btn->setStyleSheet("border-image : url(:/resources/img/mainmenu_button.png); background: transparent; color: white;");
    connect(continue_btn, SIGNAL(clicked()), this, SLOT(pause_game()));
    continue_btn->move(player->getPlayerInfo()->getX(), player->getPlayerInfo()->getY());
    continue_btn->hide();
    scene->addWidget(continue_btn);
    pauseButtons.append(continue_btn);

    Button *settings_btn = new Button("Settings");
    settings_btn->setStyleSheet("border-image : url(:/resources/img/mainmenu_button.png); background: transparent; color: white;");
    connect(settings_btn, SIGNAL(clicked()), this, SLOT(open_settings()));
    settings_btn->move(player->getPlayerInfo()->getX(), player->getPlayerInfo()->getY()+100);
    settings_btn->hide();
    scene->addWidget(settings_btn);
    pauseButtons.append(settings_btn);

    Button *quit_btn = new Button("Quit");
    quit_btn->setStyleSheet("border-image : url(:/resources/img/mainmenu_button.png); background: transparent; color: white;");
    connect(quit_btn, SIGNAL(clicked()), this, SLOT(open_menu()));
    quit_btn->move(player->getPlayerInfo()->getX(), player->getPlayerInfo()->getY()+200);
    quit_btn->hide();
    scene->addWidget(quit_btn);
    pauseButtons.append(quit_btn);

}

void Game::pause_game()
{
    if(currentState == RUNNING){
        int tmp = 0;
        gameEngine->stop();
        musicPlayer->pause();
        QPointF background_pos = mapToScene(viewport()->width()/2-pauseBackground->width()/2,
                                            viewport()->height()/2-pauseBackground->height()/2);
        pauseBackground->move(background_pos.x(),background_pos.y());
        pauseBackground->raise();
        pauseBackground->show();

        for (Button* button : pauseButtons)
        {
            button->move(pauseBackground->pos().x()+0.5*(pauseBackground->width()-BUTTON_WIDTH),
                         pauseBackground->pos().y()+0.5*(pauseBackground->height()-2*BTN_GAP-BUTTON_HEIGHT)+tmp);//, player->getPlayerInfo()->getY()+tmp);
            button->show();
            tmp += BTN_GAP;
        }

        QPixmap curs = QPixmap(":/resources/img/cursor.png");
        curs.scaled(CURSOR_SIZE, CURSOR_SIZE);
        QCursor cursor(curs);
        QApplication::setOverrideCursor(cursor);
        currentState = PAUSE;
    }
    else if(currentState == PAUSE){
        pauseBackground->hide();
        for (Button* button : pauseButtons)
        {
            button->hide();
        }
        gameEngine->start();
        if(!musicPaused)
            play_music();
        QApplication::setOverrideCursor(Qt::BlankCursor);
        currentState = RUNNING;
    }
}

void Game::remove_items()
{
    if (currentState == GAME_OVER){
        return;
    }
    for (auto &snowball : player->getAllSnowballs()) {
        if(snowball->getToBeRemoved()){
            player->getAllSnowballs().removeOne(snowball);
            delete snowball;
        }
    }
    QList<Enemy_show *> enemies = this->getLevel()->getEnemies();
    for (auto &enemy : enemies) {
        if(enemy->getToBeRemoved()){
            this->getLevel()->getEnemies().removeOne(enemy);
            delete enemy;
        }
    }
    QList<Collectable_show *> collectables = getLevel()->getCollectables();
    for (auto &collectable : collectables) {
        if(collectable->getToBeRemoved()){
            this->getLevel()->getCollectables().removeOne(collectable);
            delete collectable;
        }
    }
}

void Game::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    fitInView(0, 0, menu->width(), menu->height(),Qt::IgnoreAspectRatio);
}


void Game::playMusic(QString qrcPath)
{

    musicPlayer->setAudioOutput(audioOutput);
    musicPlayer->setSource(QUrl(qrcPath));
    audioOutput->setVolume(float(0.1));
    musicPlayer->play();
}


Game :: ~Game()
{
    delete menu;
    delete musicPlayer;
    delete scene;
    delete audioOutput;
    delete gameEngine;
    delete settings;
    delete level;
    delete dialogScreen;
//    delete snowballText;
//    delete healthText;
//    delete healthBar;
    pauseButtons.clear();
    //delete pauseBackground;
}

QGraphicsScene* Game::getScene()
{
    return scene;
}

Player_show* Game::getPlayer()
{
    return player;
}

Level* Game::getLevel() const
{
    return level;
}

Menu* Game::getMenu() const
{
    return menu;
}

game_state Game::getCurrentState() const
{
    return currentState;
}

unsigned Game::getLevelID()
{
    return levelID;
}


void Game::setCurrentState(game_state newState)
{
    currentState = newState;
}





