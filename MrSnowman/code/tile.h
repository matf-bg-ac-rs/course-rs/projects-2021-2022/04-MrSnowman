#ifndef TILE_H
#define TILE_H

#include "object.h"

class Tile : public Object
{

public:
    Tile(const double &_x, const double &_y, const bool &_isCollidable = true, const bool &_isLethal = false);
    const bool& getIsLethal() const;
    ~Tile();

private:
    bool isLethal;
    void setIsLethal(const bool &_isLethal);
};

#endif // TILE_H
