#ifndef ENEMY_SHOW_H
#define ENEMY_SHOW_H

#include <QGraphicsPixmapItem>
#include <QString>
#include "enemy.h"
#include "collidable.h"
#include "show.h"

class Enemy_show: public Show, public Collidable
{
public:
    Enemy_show(Enemy_type type, const double &x, const double &y);
    ~Enemy_show();
    Enemy* getEnemyInfo() const;
    void setImg() override;
    void animate() override;
    void collidingWithPlayer(Player_show* player) override;
    void collidingWithSnowball(SnowballAmmo_show* snowball) override;

private:

    Enemy *enemyInfo;

};

#endif // ENEMY_SHOW_H
