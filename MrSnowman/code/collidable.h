#ifndef COLLIDABLE_H
#define COLLIDABLE_H

#include "player_show.h"
#include "snowballammo_show.h"

class Collidable
{
public:
    Collidable();
    virtual void collidingWithPlayer(Player_show* player) = 0;
    virtual void collidingWithSnowball(SnowballAmmo_show* snowball) = 0;
};

#endif // COLLIDABLE_H
