#ifndef PLAYER_H
#define PLAYER_H

#include "live_entity.h"

class Player : public Live_entity
{

public:
    Player(const double &_x, const double &_y, const unsigned &_hp = 5);
    ~Player();

    void startMoving();
    void stopMoving();
    void jump();
    void calculateVerticalSpeed();
    void startFalling();
    void stopFalling();
    unsigned getAmmo() const;
    bool getIsFalling() const;
    double getJumpDuration() const;
    double getVerticalSpeed() const;
    unsigned getImunityDuration() const;
    double getFallingSpeed() const;
    bool getShouldFall() const;
    void prepareToFall();
    void unprepareToFall();
    void changeDirection(const Direction &dir);
    void increaseHealth();
    void increaseAmmo();
    void makeImune();
    void decreaseImunity();
    void throwASnowball();
    void move() override;
    void changePosition(const double &x, const double &y);

    void changeIsMoving(const bool &_isMoving);

private:

    unsigned ammo;
    double jumpDuration;
    bool isFalling;
    double verticalSpeed;
    double fallingSpeed;
    unsigned imunityDuration;
    unsigned baseImunityDuration;
    Direction movingDirection;
    bool shouldFall;
    void setAmmo(const unsigned &_ammo);
    void setFallingSpeed(const double &_fallingSpeed);
    void setVerticalSpeed(const double  &_verticalSpeed);
    void setImunityDuration(const unsigned &_imunityDuration);
    void setIsFalling(const bool &_isFalling);
    void setJumpDuration(const double &_jumpDuration);
    void setShouldFall(const double &_shouldFall);


};

#endif // PLAYER_H
