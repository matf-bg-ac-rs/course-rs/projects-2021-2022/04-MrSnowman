#ifndef LIVE_ENTITY_H
#define LIVE_ENTITY_H

#include "entity.h"

class Live_entity : public Entity
{

protected:
    void setHp(const unsigned &_hp);
    void setIsAlive(const bool &_isAlive);

public:
    Live_entity(const double &_x, const double &_y, const unsigned &_hp, const double &_speed = 10.0, const bool &_isAlive = true);
    virtual ~Live_entity();
    void takeDamage(const unsigned &dmg);
    bool checkDeath() const;
    unsigned getHP() const;

private:
    unsigned hp;
    bool isAlive;


};

#endif // LIVE_ENTITY_H
