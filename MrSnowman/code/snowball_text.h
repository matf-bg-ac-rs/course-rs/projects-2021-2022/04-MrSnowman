#ifndef SNOWBALL_TEXT_H
#define SNOWBALL_TEXT_H

#include <QGraphicsTextItem>
#include <QFont>

class Snowball_text: public QGraphicsTextItem
{
public:
    Snowball_text();
    void setSnowballNumber(unsigned number);
    unsigned getSnowballNumber();
    ~Snowball_text();
private:
    int snowballNumber;
};

#endif // SNOWBALL_TEXT_H
